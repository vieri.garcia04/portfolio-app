import { service } from './implementations/firebase/CustomerServiceFirebase'

export const saveCustomer = service.saveCustomer;

export const updateCustomer =service.updateCustomer;


export const getCustomers = service.getCustomers;

export const deleteCustomer = service.deleteCustomer;

export const getCustomer = service.getCustomer;