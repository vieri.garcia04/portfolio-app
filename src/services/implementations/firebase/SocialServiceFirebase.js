import { db } from '../../../firebase';

import {
    collection,
    addDoc,
    updateDoc,
    deleteDoc,
    doc,
    getDoc,
    getDocs,
} from "firebase/firestore";

const collectionName = "socials";


class SocialServiceFirebase{
    saveSocial = (social) =>
        addDoc(collection(db, collectionName), social);

    updateSocial = (id, updatedSocial ) =>
        updateDoc(doc(db, collectionName, id), updatedSocial);


    getSocials = () => getDocs(collection(db, collectionName));

    deleteSocial = (id) => deleteDoc(doc(db, collectionName, id));

    getSocial = (id) => getDoc(doc(db, collectionName, id));
}

export const service = new SocialServiceFirebase();
