import { db } from '../../../firebase';

import {
    collection,
    addDoc,
    updateDoc,
    deleteDoc,
    doc,
    getDoc,
    getDocs,
} from "firebase/firestore";

const collectionName = "counters";


class CounterServiceFirebase{
    saveCounter = (counter) =>
        addDoc(collection(db, collectionName), counter);

    updateCounter = (id, updatedCounter ) =>
        updateDoc(doc(db, collectionName, id), updatedCounter);


    getCounters = () => getDocs(collection(db, collectionName));

    deleteCounter = (id) => deleteDoc(doc(db, collectionName, id));

    getCounter = (id) => getDoc(doc(db, collectionName, id));
}

export const service = new CounterServiceFirebase();
