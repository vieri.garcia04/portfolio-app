import { db } from '../../../firebase';

import {
    collection,
    addDoc,
    updateDoc,
    deleteDoc,
    doc,
    getDoc,
    getDocs,
} from "firebase/firestore";

const collectionName = "prospects";


class ProspectServiceFirebase{
    saveProspect = (social) =>
        addDoc(collection(db, collectionName), social);

    updateProspect = (id, updatedProspect ) =>
        updateDoc(doc(db, collectionName, id), updatedProspect);


    getProspects = () => getDocs(collection(db, collectionName));

    deleteProspect = (id) => deleteDoc(doc(db, collectionName, id));

    getProspect = (id) => getDoc(doc(db, collectionName, id));
}

export const service = new ProspectServiceFirebase();
