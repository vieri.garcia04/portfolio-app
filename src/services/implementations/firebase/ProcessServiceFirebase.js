import { db } from '../../../firebase';

import {
    collection,
    addDoc,
    updateDoc,
    deleteDoc,
    doc,
    getDoc,
    getDocs,
} from "firebase/firestore";

const collectionName = "process";


class ProcessServiceFirebase{
    saveProcess = (step) =>
        addDoc(collection(db, collectionName), step);

    updateProcess = (id, updatedProcess ) =>
        updateDoc(doc(db, collectionName, id), updatedProcess);


    getProcesss = () => getDocs(collection(db, collectionName));

    deleteProcess = (id) => deleteDoc(doc(db, collectionName, id));

    getProcess = (id) => getDoc(doc(db, collectionName, id));
}

export const service = new ProcessServiceFirebase();
