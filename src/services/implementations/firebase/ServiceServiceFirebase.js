import { db } from '../../../firebase';

import {
    collection,
    addDoc,
    updateDoc,
    deleteDoc,
    doc,
    getDoc,
    getDocs,
} from "firebase/firestore";

const collectionName = "services";


class ServiceServiceFirebase{
    saveService = (social) =>
        addDoc(collection(db, collectionName), social);

    updateService = (id, updatedService ) =>
        updateDoc(doc(db, collectionName, id), updatedService);


    getServices = () => getDocs(collection(db, collectionName));

    deleteService = (id) => deleteDoc(doc(db, collectionName, id));

    getService = (id) => getDoc(doc(db, collectionName, id));
}

export const service = new ServiceServiceFirebase();
