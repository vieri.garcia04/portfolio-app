import { db } from '../../../firebase';

import {
    collection,
    addDoc,
    updateDoc,
    deleteDoc,
    doc,
    getDoc,
    getDocs,
} from "firebase/firestore";

const collectionName = "customers";


class CustomerServiceFirebase{
    saveCustomer = (social) =>
        addDoc(collection(db, collectionName), social);

    updateCustomer = (id, updatedCustomer ) =>
        updateDoc(doc(db, collectionName, id), updatedCustomer);


    getCustomers = () => getDocs(collection(db, collectionName));

    deleteCustomer = (id) => deleteDoc(doc(db, collectionName, id));

    getCustomer = (id) => getDoc(doc(db, collectionName, id));
}

export const service = new CustomerServiceFirebase();
