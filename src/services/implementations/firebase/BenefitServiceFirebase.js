import { db } from '../../../firebase';

import {
    collection,
    addDoc,
    updateDoc,
    deleteDoc,
    doc,
    getDoc,
    getDocs,
} from "firebase/firestore";

const collectionName = "benefits";


class BenefitServiceFirebase{
    saveBenefit = (social) =>
        addDoc(collection(db, collectionName), social);

    updateBenefit = (id, updatedBenefit ) =>
        updateDoc(doc(db, collectionName, id), updatedBenefit);


    getBenefits = () => getDocs(collection(db, collectionName));

    deleteBenefit = (id) => deleteDoc(doc(db, collectionName, id));

    getBenefit = (id) => getDoc(doc(db, collectionName, id));
}

export const service = new BenefitServiceFirebase();
