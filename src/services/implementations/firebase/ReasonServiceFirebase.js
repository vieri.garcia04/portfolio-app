import { db } from '../../../firebase';

import {
    collection,
    addDoc,
    updateDoc,
    deleteDoc,
    doc,
    getDoc,
    getDocs,
} from "firebase/firestore";

const collectionName = "reasons";


class ReasonServiceFirebase{
    saveReason = (social) =>
        addDoc(collection(db, collectionName), social);

    updateReason = (id, updatedReason ) =>
        updateDoc(doc(db, collectionName, id), updatedReason);


    getReasons = () => getDocs(collection(db, collectionName));

    deleteReason = (id) => deleteDoc(doc(db, collectionName, id));

    getReason = (id) => getDoc(doc(db, collectionName, id));
}

export const service = new ReasonServiceFirebase();
