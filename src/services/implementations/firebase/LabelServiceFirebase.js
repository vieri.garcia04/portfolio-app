import { db } from '../../../firebase';

import {
    collection,
    addDoc,
    updateDoc,
    deleteDoc,
    doc,
    getDoc,
    getDocs,
} from "firebase/firestore";

const collectionName = "labels";


class LabelServiceFirebase{
    saveLabel = (social) =>
        addDoc(collection(db, collectionName), social);

    updateLabel = (id, updatedLabel ) =>
        updateDoc(doc(db, collectionName, id), updatedLabel);


    getLabels = () => getDocs(collection(db, collectionName));

    deleteLabel = (id) => deleteDoc(doc(db, collectionName, id));

    getLabel = (id) => getDoc(doc(db, collectionName, id));
}

export const service = new LabelServiceFirebase();
