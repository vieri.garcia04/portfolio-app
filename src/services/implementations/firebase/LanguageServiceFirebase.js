import { db } from '../../../firebase';

import {
    collection,
    addDoc,
    updateDoc,
    deleteDoc,
    doc,
    getDoc,
    getDocs,
} from "firebase/firestore";

const collectionName = "languages";


class LanguageServiceFirebase{
    saveLanguage = (social) =>
        addDoc(collection(db, collectionName), social);

    updateLanguage = (id, updatedLanguage ) =>
        updateDoc(doc(db, collectionName, id), updatedLanguage);


    getLanguages = () => getDocs(collection(db, collectionName));

    deleteLanguage = (id) => deleteDoc(doc(db, collectionName, id));

    getLanguage = (id) => getDoc(doc(db, collectionName, id));
}

export const service = new LanguageServiceFirebase();
