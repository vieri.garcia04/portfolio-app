import { db } from '../../../firebase';

import {
    collection,
    addDoc,
    updateDoc,
    deleteDoc,
    doc,
    getDoc,
    getDocs,
} from "firebase/firestore";

const collectionName = "skills";


class SkillServiceFirebase{
    saveSkill = (social) =>
        addDoc(collection(db, collectionName), social);

    updateSkill = (id, updatedSkill ) =>
        updateDoc(doc(db, collectionName, id), updatedSkill);


    getSkills = () => getDocs(collection(db, collectionName));

    deleteSkill = (id) => deleteDoc(doc(db, collectionName, id));

    getSkill = (id) => getDoc(doc(db, collectionName, id));
}

export const service = new SkillServiceFirebase();
