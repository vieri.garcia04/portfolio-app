import { service } from './implementations/firebase/ProspectServiceFirebase'

export const saveProspect = service.saveProspect;

export const updateProspect =service.updateProspect;


export const getProspects = service.getProspects;

export const deleteProspect = service.deleteProspect;

export const getProspect = service.getProspect;