import { service } from './implementations/firebase/ReasonServiceFirebase'

export const saveReason = service.saveReason;

export const updateReason =service.updateReason;


export const getReasons = service.getReasons;

export const deleteReason = service.deleteReason;

export const getReason = service.getReason;