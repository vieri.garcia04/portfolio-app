import { service } from './implementations/firebase/ProcessServiceFirebase'

export const saveProcess = service.saveProcess;

export const updateProcess =service.updateProcess;


export const getProcesss = service.getProcesss;

export const deleteProcess = service.deleteProcess;

export const getProcess = service.getProcess;