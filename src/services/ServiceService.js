import { service } from './implementations/firebase/ServiceServiceFirebase'

export const saveService = service.saveService;

export const updateService =service.updateService;


export const getServices = service.getServices;

export const deleteService = service.deleteService;

export const getService = service.getService;