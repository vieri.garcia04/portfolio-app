import { service } from './implementations/firebase/LabelServiceFirebase'

export const saveLabel = service.saveLabel;

export const updateLabel =service.updateLabel;


export const getLabels = service.getLabels;

export const deleteLabel = service.deleteLabel;

export const getLabel = service.getLabel;