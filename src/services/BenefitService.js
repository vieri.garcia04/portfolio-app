import { service } from './implementations/firebase/BenefitServiceFirebase'

export const saveBenefit = service.saveBenefit;

export const updateBenefit =service.updateBenefit;


export const getBenefits = service.getBenefits;

export const deleteBenefit = service.deleteBenefit;

export const getBenefit = service.getBenefit;