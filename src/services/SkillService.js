import { service } from './implementations/firebase/SkillServiceFirebase'

export const saveSkill = service.saveSkill;

export const updateSkill =service.updateSkill;


export const getSkills = service.getSkills;

export const deleteSkill = service.deleteSkill;

export const getSkill = service.getSkill;