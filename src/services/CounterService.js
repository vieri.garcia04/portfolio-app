import { service } from './implementations/firebase/CounterServiceFirebase'

export const saveCounter = service.saveCounter;

export const updateCounter =service.updateCounter;


export const getCounters = service.getCounters;

export const deleteCounter = service.deleteCounter;

export const getCounter = service.getCounter;