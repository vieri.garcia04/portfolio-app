import axios from "axios"


export const sendEmail = async (name, message, email) => {
	try {
		// console.log(emailConnection);
        const email_api=process.env?.REACT_APP_BACKEND_HOST;
        return await axios.post(`${email_api}/email`,{
            name, message, email
        });
        
	} catch (error) {
		console.error(error);
	}
};

