import { service } from './implementations/firebase/SocialServiceFirebase'

export const saveSocial = service.saveSocial;

export const updateSocial =service.updateSocial;


export const getSocials = service.getSocials;

export const deleteSocial = service.deleteSocial;

export const getSocial = service.getSocial;