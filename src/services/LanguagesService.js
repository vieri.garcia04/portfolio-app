import { service } from './implementations/firebase/LanguageServiceFirebase'

export const saveLanguage = service.saveLanguage;

export const updateLanguage =service.updateLanguage;


export const getLanguages = service.getLanguages;

export const deleteLanguage = service.deleteLanguage;

export const getLanguage = service.getLanguage;