import React, { useEffect } from "react";
import { makeStyles, Typography, useTheme, Container } from "@material-ui/core";
import { motion, useAnimation } from "framer-motion";
import { useInView } from "react-intersection-observer";
import Divider from "../components/Divider";

const SectionContainer = ({ children, full, reverse, title, padding, paddingHorizontal, maxWidth = 'xl', showDivider = false, showTitle = true, backgroundColor, background, ...rest }) => {

    const titleControls = useAnimation();
    const contentControls = useAnimation();
    const [titleRef, titleInView] = useInView();
    const [contentRef, contentInView] = useInView();
    const theme = useTheme();
    const classes = useStyles({ full, padding, paddingHorizontal, backgroundColor, background });
    // const [contentStart, setContentStart]=useState(false)

    useEffect(() => {
        if (titleInView) {
            titleControls.start("visible");
        }
    }, [titleControls, titleInView]);

    useEffect(() => {
        if (contentInView) {
            // setContentStart(true)
            contentControls.start("visible");
        }
    }, [contentControls, contentInView]);

    return (
        <Container component="section" maxWidth={maxWidth} className={classes.container} {...rest}>
            {title && (
                <motion.div
                    ref={titleRef}
                    animate={titleControls}
                    initial="hidden"
                    transition={{
                        delay: 0.3,
                        type: "spring",
                        stiffness: 100,
                        damping: 20,
                    }}
                    variants={{
                        visible: { opacity: 1, x: 0 },
                        hidden: { opacity: 0, x: reverse ? 0 : 0 },
                    }}
                    className={classes.titleContainer}
                >
                    {showDivider && <Divider style={{ backgroundColor: theme.palette.primary.main }} width="20%" />}
                    {showTitle && <Typography variant="h5" color="initial" className={classes.title}>
                        {title}
                    </Typography>}
                    {showDivider && <Divider style={{ backgroundColor: theme.palette.primary.main }} fullWidth />}

                </motion.div>
            )}
            <motion.div
                ref={contentRef}
                animate={contentControls}
                initial="hidden"
                transition={{
                    delay: 0.5,
                    type: "spring",
                    stiffness: 100,
                    damping: 20,
                    when: "beforeChildren"
                }}
                variants={{
                    visible: { opacity: 1, y: 0 },
                    hidden: { opacity: 0, y: -50 },
                }}
            >
                {children}
            </motion.div>
        </Container>
    );
};

const useStyles = makeStyles((theme) => ({
    container: {
        paddingTop: props => props.padding ? `${props.padding}px` : 100,
        paddingBottom: props => props.padding ? `${props.padding}px` : "80px",
        paddingLeft: props => props.paddingHorizontal !== undefined ? props.paddingHorizontal : 20,
        paddingRight: props => props.paddingHorizontal !== undefined ? props.paddingHorizontal : 20,
        background: props => props.background || props.backgroundColor || '#FFF',
        backgroundSize:'cover',
        backgroundPosition:'center'
    },
    titleContainer: {
        paddingBottom: theme.spacing(8),
        display: "flex",
        alignItems: "center",
        maxWidth: "100%"
    },
    title: {
        margin: theme.spacing(0, 4),
        whiteSpace: "nowrap"
    },
    childrenContainer: {
        minHeight: "100%",
    }
}));

export default SectionContainer;
