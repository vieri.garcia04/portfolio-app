import { useContext, useState, useEffect } from "react";
import { makeStyles, Container, Box } from "@material-ui/core";
import ThemeContext from "../contexts/themeContext";
import { getSourceUrl } from "../utils/storageUtils";


const HomeContainer = ({ children, ...rest }) => {
    const { isDarkMode } = useContext(ThemeContext);
    const [homeBackground, setHomeBackground] = useState('');
    const props = {
        mode: isDarkMode ? 'dark' : 'light',
        homeBackground,

    }
    const classes = useStyles(props)();
    useEffect(() => {
        getSourceUrl('images/background.jpg', setHomeBackground);
    }, [])


    return (
        <Box className={classes.content} {...rest}>
            {children}
        </Box>
    );
};

const useStyles = props => makeStyles((theme) => {
    return (
        {
            content: {

                display: "flex",
                flexDirection: 'column',
                justifyContent: 'center',
                height: '97vh',
                width: '100%',
                flexGrow: 1,
                paddingTop: `calc( ${theme.spacing(2)}px + ${theme.navbarHeight} ) `,
                paddingLeft: theme.spacing(15),
                paddingRight: theme.spacing(15),
                background: `linear-gradient(0deg, rgba(4, 4, 4, 0.3), rgba(4, 4, 4, 0.3)), url(${props.homeBackground})`,
                backgroundSize: 'cover',
                backgroundPosition: 'center center',
                backgroundRepeat: 'no-repeat',
                [theme.breakpoints.down('sm')]: {
                    paddingTop: theme.navbarHeight,
                    alignItems: 'center',

                },
            }
        })
});

export default HomeContainer;
