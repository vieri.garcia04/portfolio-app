import link2goal from "./assets/images/link2goal.png"
import siscaeImage from "./assets/images/siscae.png"
import siscaeImage2 from "./assets/images/siscae_2.png"
import epowerlineImage from "./assets/images/epowerline.jpeg"
import image4 from "./assets/images/shopifyErp.png"
import image1 from "./assets/images/vg_landingpage.png"
import image2 from "./assets/images/ofima.png"
import image3 from "./assets/images/moneymentor.png"

const reasonsList = [
    {
        'icon': 'https://img.icons8.com/color/96/null/environment-care.png',
        'title': {
            'en': 'Position your brand around the world',
            'es': 'Posiciónate en todo el mundo'
        },
        'description': {
            'en': 'Create your personalized website that offers your products and services around the world, and improves your corporate image.',
            'es': 'Crea tu sitio web personalizado que oferte tus productos y servicios alrededor del mundo, y mejore tu imagen corporativa.'
        }
    }, {
        'icon': 'https://img.icons8.com/color/96/null/low-price.png',
        'title': {
            'en': 'Reduce costs up to 90%',
            'es': 'Reduce costos hasta en un 90%'
        },
        'description': {
            'en': 'With automated processes, improve your workflows, eliminate costs due to errors or unnecessary tasks.',
            'es': 'Con procesos automatizados mejora tus flujos de trabajo, elimina costos producto de errores o tareas innecesarias.'
        }
    }, {
        'icon': 'https://img.icons8.com/color/96/null/in-love--v1.png',
        'title': {
            'en': 'Your clients will love you',
            'es': 'Gánate el amor de tus clientes'
        },
        'description': {
            'en': 'Know better the behavior, tastes and interests of your customers. This way you will offer them what they need when they need it.',
            'es': 'Conoce mejor el comportamiento, gustos e interéses de tus clientes. Así les ofrecerás lo que necesitan cuando lo necesitan.'
        }
    }, {
        'icon': 'https://img.icons8.com/color/96/null/brainstorm-skill.png',
        'title': {
            'en': 'Make the best decisions',
            'es': 'Toma las mejores decisiones'
        },
        'description': {
            'en': 'Collect and analyze data about your business and your customers, so you can find opportunities for improvement and growth.',
            'es': 'Recopila y analiza datos sobre tu negocio y tus clientes, así podrás encontrar oportunidades de mejora y de crecimiento.'
        }
    }, {
        'icon': 'https://img.icons8.com/color/96/null/positive-dynamic.png',
        'title': {
            'en': 'Increase your Productivity',
            'es': 'Aumenta tu Productividad '
        },
        'description': {
            'en': 'With automated processes you will be able to satisfy more customers in less time and increase your productivity by up to 84%.',
            'es': 'Con procesos automatizados podrás satisfacer a más clientes en menos tiempo e incrementar tu productividad hasta en un 84%.'
        }
    }, {
        'icon': 'https://img.icons8.com/color/96/null/growing-money--v2.png',
        'title': {
            'en': 'Increase your profit',
            'es': 'Incrementa tus ganancias'
        },
        'description': {
            'en': 'With reach to more customers and more efficient processes, make the income of your business take off.',
            'es': 'Con alcance a más clientes y procesos más eficientes haz despegar los ingresos de tu negocio.'
        }
    }
]

const servicesList = [
    {
        'icon': 'https://img.icons8.com/color/96/null/internet--v1.png',
        'title': {
            'en': 'Systems Consulting',
            'es': 'Consultoría de Sistemas'
        },
        'description': {
            'en': 'We analyze your business, identify opportunities for improvement and propose technological solutions designed exclusively for you.',
            'es': 'Analizamos tu negocio, identificamos oportunidades de mejora y proponemos soluciones tecnológicas diseñadas exclusivamente para ti.'
        }
    }, {
        'icon': 'https://img.icons8.com/color/96/null/web.png',
        'title': {
            'en': 'Web Solutions',
            'es': 'Soluciones Web'
        },
        'description': {
            'en': 'Make your business known and what it can offer on your own website and landing page, increase your sales and provide a 24/7 service with your own E-commerce',
            'es': 'Da a conocer tu negocio y lo que puede ofrecer en tu propio sitio web y página de aterrizaje, incrementa tus ventas y brinda un servicio 24/7 con tu propio  E-commerce'
        }
    }, {
        'icon': 'https://img.icons8.com/color/96/null/artificial-intelligence.png',
        'title': {
            'en': 'Data Analytics',
            'es': 'Analítica de datos'
        },
        'description': {
            'en': 'Collect and make the most of your business data and transform it into valuable information to make decisions that allow you to achieve your goals.',
            'es': 'Recopila y aprovecha al máximo los datos de tu negocio y transfórmalos en información valiosa para tomar decisiones que te permitan lograr tus objetivos.'
        }
    }, {
        'icon': 'https://img.icons8.com/color/96/null/share-2.png',
        'title': {
            'en': 'Systems Integrations',
            'es': 'Integraciones de Sistemas'
        },
        'description': {
            'en': 'We create ties that allow you to connect your systems with someone else that adds value to your business.',
            'es': 'Creamos lazos que permitan conectar tus sistemas con algún otro que aporte valor a tu negocio.'
        }
    }, {
        'icon': 'https://firebasestorage.googleapis.com/v0/b/vierigarcia-blog.appspot.com/o/icons%2Fhome%2FAndroid%20OS.png?alt=media&token=8fd87255-7913-4aea-b9ab-cd9d0f5ca18e',
        'title': {
            'en': 'Apps',
            'es': 'Apps'
        },
        'description': {
            'en': 'We bring your business within reach of your customers\' hands, we develop mobile applications that provide a close and quality experience.',
            'es': 'Llevamos tu negocio al alcance de las manos de tus clientes, desarrollamos aplicaciones móviles que proporcionen una experiencia cercana y de calidad.'
        }
    }, {
        'icon': 'https://img.icons8.com/color/96/null/automatic.png',
        'title': {
            'en': 'ERP',
            'es': 'ERP'
        },
        'description': {
            'en': 'Automate the main processes of your business, reduce your costs and increase your productivity and your profits. We build an ERP designed for you.',
            'es': 'Automatiza los principales procesos de tu negocio, reduce sus costos e incrementa su produtividad y tus ganancias. Construimos una ERP pensada para ti.'
        }
    }
]

const skillsList = [{
    'description': { 'en': 'Project Management', 'es': 'Gestión de Projectos' },
    'icon': 'Facebook'
},
{ 'description': { 'en': 'Leadership', 'es': 'Liderazgo' }, 'icon': 'Facebook' },
{
    'description': {
        'en': 'Web and mobile development',
        'es': 'Desarrollo Web & Móvil'
    },
    'icon': 'Facebook'
},
{
    'description': { 'en': 'APIS development', 'es': 'Desarrollo de APIs' },
    'icon': 'Facebook'
},
{ 'description': { 'en': 'Python', 'es': 'Python' }, 'icon': 'Facebook' },
{
    'description': { 'en': 'Effective communication', 'es': 'Comunicación efectiva' },
    'icon': 'Facebook'
},
{ 'description': { 'en': 'Javascript', 'es': 'Javascript ' }, 'icon': 'Facebook' },
{ 'description': { 'en': 'Empathy', 'es': 'Empatía' }, 'icon': 'Facebook' },
];

const projectList = [
    {
        "id": 1,
        "title": "Vieri García",
        "overview": {
            "en": "Corporative landing page",
            "es": "Landing Page corporativa"
        },
        "tag": {
            "en": "CORPORATIVE SITE",
            "es": "WEB CORPORATIVA"
        },
        "images": [image1]
    },
    
    {
        "id": 2,
        "title": "SISCAE",
        "overview": {
            "en": "Study Areas Control System",
            "es": "Sistema de Control de Áreas de Estudio"
        },
        "tag": {
            "en": "WEB SOLUTION",
            "es": "SOLUCIÓN WEB"
        },
        images: [siscaeImage,siscaeImage2]
    },
    {
        id: 3,
        title: "Epowerline",
        overview: {
            "en": "Electrical network viewer system",
            "es": "Sistema visor de redes eléctricas"
        },
        images: [epowerlineImage],
        "tag": {
            "en": "GEOGRAPHIC INFORMATION SOLUTION",
            "es": "SISTEMA DE INFORMACIÓN GEOGRÁFICA"
        },
    },
    {
        id: 4,
        title: "Ofima",
        overview: {
            "en": "Own Finances Manager Software",
            "es": "Software Gestor de Finanzas Personales"
        },
        "tag": {
            "en": "WEB SOLUTION",
            "es": "SOLUCIÓN WEB"
        },
        images: [image2]
    },
    {
        "id": 5,
        "title": "Link2goal",
        "overview": {
            "en": "Strategic Planning and Control System",
            "es": "Sistema de planeamiento y control estratégico"
        },
        "tag": {
            "en": "WEB SOLUTION",
            "es": "SOLUCIÓN WEB"
        },
        "images": [link2goal]
    },
    {
        id: 6,
        title: "Shopify ERP",
        overview: {
            "en": "Module that integrates Yuju with the Shopify marketplace",
            "es": "Módulo que integra Yuju al marketplace Shopify"
        },
        "tag": {
            "en": "API INTEGRATION",
            "es": "INTEGRACIÓN CON API SHOPIFY"
        },
        images: [image4]
    },{
        id: 7,
        title: "Money Mentor API",
        overview: {
            "en": "Swagger-documented RESTful API for Money Mentor",
            "es": "API REST documentada en Swagger de Money Mentor"
        },
        "tag": {
            "en": "REST API",
            "es": "API REST"
        },
        images: [image3]
    },


];

const projectImagesList =[]
projectList.forEach((project) => {
    project.images.forEach((image) => {
        projectImagesList.push({ ...project, image })
    })
})


const experienceList = [
    {
        id: 0,
        company: "YUJU",
        job: {
            "en": "Python developer & Channel manager",
            "es": "Desarrollador Python y Encargado de canal"
        },
        duration: {
            "en": "December 2021 - Present",
            "es": "Diciembre 2021 - Actualidad"
        },
        overview: {
            "es": "Yuju es una solución (Saas) por medio de la cual podrás vender desde una sola interfaz en los marketplaces más grandes de America Latina de forma organizada. Ayudan a las empresas a aumentar sus ventas en los mejores ​canales online, optimizar y automatizar sus procesos.",
            "en": "Yuju is a solution (Saas) through which you can sell from a single interface in the largest marketplaces in Latin America in an organized way. They help companies increase their sales in the best online channels, optimize and automate their processes."
        },
        links: {
            "website": "https://yuju.io"
        }
    },
    {
        id: 1,
        company: "BITZONE",
        job: {
            "en": "Project Manager & SCRUM Product Owner",
            "es": "Gestor de Proyectos y Product Owner"
        },
        duration: {
            "en": "December 2020 - October 2021",
            "es": "Diciembre 2020 - Octubre 2021"
        },
        overview: {
            "es": "Bitzone es una empresa dedicada a ofrecer servicios de consultoría y soluciones TI. Es un espacio donde la opinión de cada uno de sus miembros cuenta y que brinda oportunidades de crecimiento y mejora.",
            "en": "Bitzone is a company dedicated to offering consulting services and IT solutions. It is a space where the opinion of each of its members counts and that provides opportunities for growth and improvement."
        },
        links: {
            "instagram": "https://www.instagram.com/bitzone.lat/"
        }
    },
    {
        id: 2,
        company: "OPENLAB",
        job: {
            "en": "Full stack developer",
            "es": "Desarrollador Full stack"
        },
        duration: {
            "en": "October 2020 - December 2020",
            "es": "Octubre 2020 - Diciembre 2020"
        },
        overview: {
            "es": "OpenLab es un espacio para compartir, aprender y buscar nuevas oportunidades en el mundo de la tecnología.",
            "en": "OpenLab is a space to share, learn and seek new opportunities in the world of technology."
        },
        links: {
        },
    },
    {
        id: 3,
        company: "ODYBANK",
        job: {
            "en": "Full stack developer & Project Leader",
            "es": "Desarrollador Full stack y Líder de Proyectos"
        },
        duration: {
            "en": "November 2018 - September 2019",
            "es": "Noviembre 2018 - Septiembre 2019"
        },
        overview: {
            "es": "Es una empresa que ofrece servicios de consultoría, capacitación y desarrollo de software a instituciones financieras pertenecientes a la industria de medios de pago electrónicos, basados en el procesamiento de tarjetas de crédito, débito y prepago. ",
            "en": "It is a company that offers consulting, training and software development services to financial institutions belonging to the industry of electronic means of payment, based on the processing of credit, debit and prepaid cards."
        },
        links: {
            website: "https://www.odybank.com.pe"
        },
    },
    {
        id: 4,
        company: "UNMSM-FISI",
        job: {
            "en": "Project Manager & Full stack developer",
            "es": "Gestor de Proyecto y desarrollador Full stack"
        },
        duration: {
            "en": "July 2018 - November 2018",
            "es": "Julio 2018 - Noviembre 2018"
        },
        overview: {
            "es": "La unidad de biblioteca de la FISI, es un área encargada de facilitar recursos bibliográficos y espacios de estudio a los estudiantes de la facultad. Es un área que promueve la innovación y desarrollo, dando oportunidad a personas como yo para desarrollar herramientas útiles a toda la comunidad estudiantil.",
            "en": "The library unit of the FISI is an area in charge of facilitating bibliographic resources and study spaces for the students of the faculty. It is an area that promotes innovation and development, giving people like me the opportunity to implement useful tools for the entire student community."
        },
        links: {
            "website": "https://sistemas.unmsm.edu.pe/site/pregrado/ingenieria-de-sistemas",
            "linkedin": "https://www.linkedin.com/school/unmsm/"
        },
    },
];

const counterLits = [
    {
        'limit': 8,
        'speed': 150,
        'title': {
            'es': 'Proyectos',
            'en': 'Projects'
        }
    }, {
        'limit': 7,
        'speed': 150,
        'title': {
            'es': 'Sistemas Activos',
            'en': 'Active Systems'
        }
    }, {
        'limit': 200,
        'speed': 2,
        'title': {
            'es': 'Usuarios Satisfechos',
            'en': 'Satisfied Users'
        },
        'prefix': '+'
    }
]

const process = [
    {
        'icon': 'https://img.icons8.com/material/96/ffffff/search--v1.png',
        'title': {
            'es': 'Análisis y propuesta',
            'en': 'Analysis and proposal'
        },
        'description': {
            'es': 'Nos ponemos en tus zapatos, identificamos lo que necesitas para mejorar y te proponemos alternativas de solución hechas a tu medida.',
            'en': 'We put ourselves in your shoes, we identify what you need to improve and we propose alternative solutions tailored to your needs.'
        },
        'order': 1
    }, {
        'icon': 'https://img.icons8.com/material/96/ffffff/source-code--v1.pn',
        'title': {
            'es': 'Implementación',
            'en': 'Development'
        },
        'description': {
            'es': 'Construimos tu solución de forma iterativa, validando paso a paso que cumpla tus expectativas y satisfaga tus necesidades, entregandola oportunamente.',
            'en': 'We build your solution iteratively, validating step by step that it meets your expectations and meets your needs, delivering it on time.'
        },
        'order': 2
    }, {
        'icon': 'https://img.icons8.com/ios-glyphs/90/ffffff/handshake--v1.png',
        'title': {
            'es': 'Entrega',
            'en': 'Delivery'
        },
        'description': {
            'es': 'Nuestro vínculo no termina en la entrega, todos nuestros clientes forman parte de nuestra comunidad, compartimos contigo información de valor y te ayudamos a obtener visibilidad. ¡Queremos verte triunfar!',
            'en': 'Our bond does not end with delivery, all our customers are part of our community, we share valuable information with you and help you gain visibility. We want to see you succeed!'
        },
        'order': 3
    }
]

const customersList = [{
    'name': 'Luigui',
    'lastname': 'Astohuaman',
    'company_name': 'BITZONE',
    'service': '',
    'testimonial': {
        'es': '.',
        'en': ''
    },
    'icon': 'https://firebasestorage.googleapis.com/v0/b/vierigarcia-blog.appspot.com/o/icons%2Fcustomers%2Fbitzone_icon.png?alt=media&token=bb5d98ca-d64c-4f0d-b079-9c1c87290094',
    'hoverIcon': 'https://firebasestorage.googleapis.com/v0/b/vierigarcia-blog.appspot.com/o/icons%2Fcustomers%2Fbitzone_icon_hover.png?alt=media&token=b74b158c-050e-434f-9a03-749f2fc6415f'
}, {
    'name': '',
    'lastname': '',
    'service': '',
    'company_name': 'ENEL PERÚ',
    'testimonial': {
        'es': '',
        'en': ''
    },
    'icon': 'https://firebasestorage.googleapis.com/v0/b/vierigarcia-blog.appspot.com/o/icons%2Fcustomers%2Fenel_icon.png?alt=media&token=3e26d311-ff90-4da1-8485-7a73bb43df4f',
    'hoverIcon': 'https://firebasestorage.googleapis.com/v0/b/vierigarcia-blog.appspot.com/o/icons%2Fcustomers%2Fenel_icon_hover.png?alt=media&token=f2746a31-d110-4acf-9c40-786c5209b2c2'
}, {
    'name': '-',
    'lastname': '-',
    'company_name': 'YUJU',
    'service': '',
    'testimonial': {
        'es': 'Yuju es un software que integra la gestión de almacen y ventas de distintos marketplace en un solo lugar, trabajando juntos se logró integrar Yuju con Shopify y Paris.',
        'en': 'Yuju is a software that integrates the warehouse and sales management of different marketplaces in one place, working together it was possible to integrate Yuju with Shopify and Paris.'
    },
    'icon': 'https://firebasestorage.googleapis.com/v0/b/vierigarcia-blog.appspot.com/o/icons%2Fcustomers%2Fyuju_icon.png?alt=media&token=6aa03f08-d9bd-415b-857f-d02f062dc75e',
    'hoverIcon': 'https://firebasestorage.googleapis.com/v0/b/vierigarcia-blog.appspot.com/o/icons%2Fcustomers%2Fyuju_icon_hover.png?alt=media&token=60d09e10-f555-4ed0-9c67-02e0e1ad14ed'
}, {
    'name': '',
    'lastname': '',
    'service': '',
    'company_name': 'SENATI-ICA',
    'testimonial': {
        'es': 'Inicialmente el proceso de matrícula de Senati se hacía de forma manual, esto conllevaba muchos errores que afectaban a sus estudiantes, junto a Bitzone se propuso Gacella para dar solución a esto.',
        'en': 'Initially, Senati\'s enrollment process was done manually, this entailed many errors that affected its students, together with Bitzone, Gacella proposed to solve this'
    },
    'icon': 'https://firebasestorage.googleapis.com/v0/b/vierigarcia-blog.appspot.com/o/icons%2Fcustomers%2Fsenati_icon.png?alt=media&token=e4e5fbd8-ddd3-4128-86c8-12f83b5f9456',
    'hoverIcon': 'https://firebasestorage.googleapis.com/v0/b/vierigarcia-blog.appspot.com/o/icons%2Fcustomers%2Fsenati_icon_hover.png?alt=media&token=dcde6c66-8e12-4831-9767-79efce4cbb32'
}]

export { skillsList, projectList, experienceList, reasonsList, servicesList, counterLits, process, customersList, projectImagesList };
