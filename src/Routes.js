import React, { lazy } from "react";

import { Switch, Route } from "react-router-dom";
const MainHome = lazy(() => import("./features/site/pages/MainHome"));
const MainAdmin = lazy(() => import("./features/admin/pages/MainAdmin"));

const Routes = () => {

    return (
        <Switch>
            <Route exact path="/" component={MainHome} />
            <Route exact path="/admin" component={MainAdmin} />
        </Switch>
    );
};

export default Routes;
