import { ref, getDownloadURL } from "firebase/storage";
import { storage } from '../firebase'

export const getSourceUrl = async (source, stateSetter) => {
    const url = await getDownloadURL(ref(storage, source))
    stateSetter(url);
}