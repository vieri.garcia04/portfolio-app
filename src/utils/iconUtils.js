import  * as MaterialDesign from "@material-ui/icons";

export const getMaterialIconByName = (iconName='Public', iconClass) => { 
    const MaterialIcon= MaterialDesign[iconName]
     return <MaterialIcon className={iconClass}/>
 }