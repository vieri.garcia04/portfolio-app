export const partList = (list, partSize) => {
  const arrayOfParts = [];
  for (let i = 0; i < list.length; i += partSize) {
    let part = list.slice(i, i + partSize);
    arrayOfParts.push(part);
  }
  return arrayOfParts;
};


export const sortArray = (list, key, asc = true) => {
  let aux = asc ? 1 : -1;
  return list.sort(
    (p1, p2) => (p1[key] < p2[key]) ? -1 * aux : (p1[key] > p2[key]) ? 1 * aux : 0);
};
