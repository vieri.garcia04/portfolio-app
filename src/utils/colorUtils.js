export const hexToRgb = (hexColor, opacy=1) => {
    var aRgbHex = hexColor.slice(1).match(/.{1,2}/g);
    var aRgb = [
        parseInt(aRgbHex[0], 16),
        parseInt(aRgbHex[1], 16),
        parseInt(aRgbHex[2], 16)
    ];
   let rgb=`rgb(${aRgb[0]},${aRgb[1]},${aRgb[2]},${opacy})`;
   return rgb;
}