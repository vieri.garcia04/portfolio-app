import { Typography } from '@material-ui/core';
import React from 'react'
import { useEffect } from 'react';
import { useState } from 'react'

const AutomaticCounter = ({ limit, start = false, speed = 50, prefix, ...TypographyProps }) => {


    const [counter, setCounter] = useState(0);
    useEffect(() => {
        if (counter < limit && start) {
            setTimeout(() => {
                setCounter(counter + 1)
            }, speed);

        }
    }, [counter, start])


    return (
        <Typography {...TypographyProps}>
            {counter === limit ? prefix : ''}{counter}
        </Typography>
    )
}

export default AutomaticCounter