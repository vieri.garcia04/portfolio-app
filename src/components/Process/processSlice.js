import { createSlice } from '@reduxjs/toolkit'
import { sortArray } from '../../utils/arrayUtils';



export const processSlice = createSlice(
    {
        name: 'process',
        initialState: {
            'data': []
        },
        reducers: {
            setProcess: (state, action) => {
                let { payload } = action
                payload=sortArray(payload,'order')
                state['data'] = payload;
            }
        }
    }
)
export const { setProcess } = processSlice.actions
export default processSlice.reducer