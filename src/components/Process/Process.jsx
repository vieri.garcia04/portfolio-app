import {  makeStyles, useMediaQuery, Container, useTheme } from "@material-ui/core";
import { useTranslation } from "react-i18next";
import { useSelector } from 'react-redux'
import InfoCard from "../InfoCard/InfoCard"


const Process = ({ animate, isDarkMode }) => {
    const theme = useTheme();
    const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
    const process = useSelector(state => state.process.data)
    const classes = useStyles({ isMobile, isDarkMode })();
    const { t } = useTranslation();


    return (
        <Container className={classes.container}>
            {process.map((elem, k) => (
                <Step
                    key={k}
                    stepTitle={elem.title[t('language')]}
                    stepDescription={elem.description[t('language')]}
                    stepIcon={elem.icon}
                    isMobile={isMobile}
                />
            ))}
        </Container>

    );
};

const Step = ({ stepIcon, stepTitle, stepDescription, key, isMobile }) => {
    const theme = useTheme()
    return (
        <InfoCard
            title={stepTitle}
            description={stepDescription}
            icon={stepIcon}
            key={key}
            containerStyle={{
                boxShadow: '0px 0px 0px rgba(0, 0, 0, 0.25)',
                justifyContent:isMobile?'center':'top',
                width: '100%',
                [theme.breakpoints.down("sm")]: {
                    alignItems: 'center',
                    '&:hover': {
                      width: '100%'
                    },
                  },
                  [theme.breakpoints.down("xs")]: {
                    '&:hover': {
                      width: '100%'
                    }
                }
            }}
            titleStyle={{
                color: 'white'
            }}
            descStyle={{
                color: 'white'
            }}
        />
    )
}
const useStyles = props => makeStyles((theme) => ({
    container: {
        width: '100%',
        height: props.isMobile ? 'auto' : 300,
        display: 'flex',
        flexDirection: props.isMobile ? 'column' : 'row',
        alignItems: props.isMobile ? 'center' : 'none',
        gap: props.isMobile ? 5 : 15,
        padding: 0
    },
    step: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 12,
        flexWrap: 'wrap',
        width: 400,
        height: 300,
        padding: 5,
        marginTop: 20,
        gap: 15,
        boxShadow: '0px 4px 30px rgba(0, 0, 0, 0.25)',
        [theme.breakpoints.down("sm")]: {
            width: 400,
        },
        [theme.breakpoints.down("xs")]: {
            width: 350
        },
        '&:hover': {
            width: 430,
            height: 330,
        },

    },
    stepTitle: {
        textAlign: 'center',
        width: '100%',
        [theme.breakpoints.down("xs")]: {
            fontSize: theme.typography.h6.fontSize,
        },
    },
    stepDescription: {
        textAlign: 'center',
        width: '100%',
        [theme.breakpoints.down("xs")]: {
            fontSize: theme.typography.titleSmall.fontSize,
        },
    },
    process: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    }
}));

export default Process;
