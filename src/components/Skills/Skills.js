import React, { useContext } from "react";
import { Typography, makeStyles, Container, useMediaQuery, useTheme } from "@material-ui/core";
//import ProgressBar from "./ProgressBar";
import { useTranslation } from "react-i18next";
import themeContext from "../../contexts/themeContext";
import { getMaterialIconByName } from "../../utils/iconUtils";
import { useSelector } from "react-redux"

// function LinearProgressWithLabel({ title, value }) {
//     const classes = useStyles();
//     return (
//         <div className={classes.skillWrapper}>
//             <Typography variant="body2" display="inline" component="span" className={classes.skillTitle}>
//                 {title}
//             </Typography>
//             <Box display="flex" alignItems="center" mb={2}>
//                 <Box width="100%" mr={1}>
//                     <ProgressBar value={value} />
//                 </Box>
//             </Box>
//         </div>
//     );
// }

const Skills = () => {
    const theme = useTheme();
    const { t } = useTranslation();
    const { isDarkMode } = useContext(themeContext)
    const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
    const classes = useStyles({ isDarkMode })();
    const skillsList = useSelector(state => state.skills.data)
    return (
        <div className={classes.container}>
            {skillsList.map((elem, k) => (
                // <Paper elevation={10} key={k} className={classes.paper}>
                // </Paper>
                <Container key={k} className={classes.skill}>
                    {getMaterialIconByName(elem.icon, classes.icon)}
                    <Typography variant={isMobile?"labelSmall":"titleLarge"}  className={classes.skillDesc} >
                        {elem?.description[t("language")]}
                    </Typography>
                </Container>
                /* {LinearProgressWithLabel(elem)} */
            ))}
        </div>
    );
};

const useStyles = props => makeStyles((theme) => ({
    container: {
        width:700,
        maxWidth: '100%',
        display: "flex",
        flexWrap: "wrap",
        flexDirection: 'row',
        alignItems: 'flex-start',
        gap: props.isMobile ? 5 : 20,
        margin:5

    },
    skillWrapper: {
        width: "100%",
    },
    skillTitle: {
        whiteSpace: "nowrap",
        marginRight: theme.spacing(1),
    },
    paper: {
        marginRight: "10px",
        marginBottom: "10px",
        minWidth: "50px",
        padding: "10px"
    },
    icon: {
        color: props.isDarkMode ? theme.palette.black : theme.palette.basicColor.primary,
        transition: "0.1s",
        cursor: "pointer",
        fontSize: "50px",
        [theme.breakpoints.down("xs")]: {
            fontSize: "30px",
        },
        marginBottom: props.isMobile ? '4px' : '8px',
    },
    skill: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: props.isMobile ? 'center' : 'left',
        flexWrap: 'wrap',
        width: 150,
        [theme.breakpoints.down("sm")]: {
            width: 250,
        },
        [theme.breakpoints.down("xs")]: {
            width: 80
        },

    },
    skillDesc: {
        textAlign: props.isMobile ? 'center' : 'left',
        color: props.isDarkMode ? theme.palette.black : theme.palette.basicColor.primary,
        
    },
}));

export default Skills;
