import { createSlice } from '@reduxjs/toolkit'

export const skillSlice = createSlice(
    {
        name: 'skills',
        initialState: {
            'data':[]
        },
        reducers: {
            setSkills: (state, action) => {
                const { payload } = action
                state['data'] = payload;
            }
        }
    }
)
export const { setSkills } = skillSlice.actions
export default skillSlice.reducer