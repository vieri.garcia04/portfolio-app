import { Typography, makeStyles, useMediaQuery, Container, useTheme, Box } from "@material-ui/core";
import { motion } from "framer-motion";
import { useTranslation } from "react-i18next";
import { useSelector } from 'react-redux'
import { getMaterialIconByName } from "../../utils/iconUtils";


const Benefits = ({ animate, isDarkMode }) => {
    const theme = useTheme();
    const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
    const benefits = useSelector(state => state.benefits.data)
    const classes = useStyles({ isMobile, isDarkMode })();
    const { t } = useTranslation();


    return (
        <motion.div
            className={classes.container} animate={animate}>
            {benefits && benefits.map((elem, k) => (
                <Container key={k} className={classes.benefit}    >
                    {getMaterialIconByName(elem.icon, classes.icon)}
                    <Typography className={classes.benefitDescription} variant="titleLarge">
                        {elem.description[t('language')]}
                    </Typography>
                </Container>
            ))}
        </motion.div>
    );
};

const useStyles = props => makeStyles((theme) => ({
    container: {
        display: "flex",
        flexDirection: 'row',
        alignItems:'flex-start',
        justifyContent:'center',
        width: '100%',
        flexWrap: 'wrap',
        marginBottom: props.isMobile ? '30px' : '20px',
        marginTop: theme.spacing(props.isMobile ? 1 : 2),
        gap:2
    },
    benefit: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: props.isMobile ? 'center' : 'left',
        flexWrap: 'wrap',
        width:  '30%',
        [theme.breakpoints.down("sm")]: {
            width:  250,
        },
        [theme.breakpoints.down("xs")]: {
            width:  80
        },
        

    },
    benefitDescription: {
        textAlign:props.isMobile ? 'center' : 'left',
        color: props.isDarkMode ? theme.palette.black : theme.palette.basicColor.primary,
        width:'100%',
        [theme.breakpoints.down("sm")]: {
            fontSize: theme.typography.labelMedium.fontSize,
            fontWeight: 300
        },
        [theme.breakpoints.down("xs")]: {
            fontSize: theme.typography.labelMedium.fontSize,
            fontWeight: 300
        },
    },
    icon: {
        color: props.isDarkMode ? theme.palette.black : theme.palette.basicColor.primary,
        transition: "0.1s",
        cursor: "pointer",
        fontSize: props.isMobile ? 35 : 45,
        marginBottom: props.isMobile ? '3px' : '8px',
    },
}));

export default Benefits;
