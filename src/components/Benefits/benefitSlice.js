import { createSlice } from '@reduxjs/toolkit'

export const benefitSlice = createSlice(
    {
        name: 'benefits',
        initialState: {
            'data':[]
        },
        reducers: {
            setBenefits: (state, action) => {
                const { payload } = action
                state['data'] = payload;
            }
        }
    }
)
export const { setBenefits } = benefitSlice.actions
export default benefitSlice.reducer