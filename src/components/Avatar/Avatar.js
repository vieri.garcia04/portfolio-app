import React from "react";
import { Box, makeStyles, Typography } from "@material-ui/core";
import AvatarImg from "../../assets/images/avatar.jpg";
import { useTranslation } from "react-i18next";

const Avatar = () => {
  const classes = useStyles();
  const { t } = useTranslation();
  return (
    <Box className={classes.container}>
      <img src={AvatarImg} alt="Avatar" className={classes.avatarImg} />
      <Typography variant="h5" fontWeight={500} color="primary" className={classes.avatarDesc}>
        {t("avatar_desc")}
      </Typography>
    </Box>

  );
};

const useStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    flexWrap: 'wrap',
  },
  avatarImg: {
    borderRadius: 20,
    width: "250px",
    height: "320px",
    objectFit: 'cover',
    objectPosition: "0 -0px",
    boxShadow: theme.shadows[5]
  },
  avatarDesc: {
    padding: 15,
    textAlign:'center'
  },
}));

export default Avatar;
