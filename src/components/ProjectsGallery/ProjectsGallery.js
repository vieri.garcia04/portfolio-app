import React, { useState } from "react";
import { AnimatePresence, AnimateSharedLayout } from "framer-motion";
import {  Container, makeStyles } from "@material-ui/core";
import Card from "./Card";
import ExtendedCard from "./ExtendedCard";
import { projectList } from "../../data";
import { useTranslation } from "react-i18next";

const ProjectsGallery = () => {
    const classes = useStyles();
    const { t } = useTranslation();
    const [selectedItem, setSelectedItem] = useState(null);

    //const getSelected = (id) => projectList.find((elem) => elem.id === id);
    return (
        <AnimateSharedLayout type="crossfade">
            {/* <Grid container  spacing={2} className={classes.galleryContainer}>
                {projectList.map((item, k) => (
                    <Grid
                        item
                        xs={12}
                        sm={6}
                        md={4}
                        key={item.id}
                        classes={{ item: classes.item }}
                        >
                        <Card
                            id={item.id}
                            title={item.title}    
                            overview={item?.overview[ t("language")] || item?.overview['en']}
                            backgroundImage={item.backgroundImage}
                            frontImage={item.frontImage}
                            technologies={item.technologies}
                            onClick={() => setSelectedItem(item)}
                            initial={{ opacity: 0, y: -50 }}
                            animate={{ opacity: 1, y: 0 }}
                        />
                    </Grid>
                ))}
            </Grid> */}
            <Container className={classes.galleryContainer}>
                {projectList.map((item, k) => (
                    <Card
                    id={item.id}
                    title={item.title}
                    overview={item?.overview[t("language")] || item?.overview['en']}
                    backgroundImage={item.backgroundImage}
                    frontImage={item.frontImage}
                    technologies={item.technologies}
                    onClick={() => setSelectedItem(item)}
                    initial={{ opacity: 0, y: -50 }}
                    animate={{ opacity: 1, y: 0 }}
                />

                ))}
            </Container>
            <AnimatePresence>
                {
                    selectedItem && (
                        <ExtendedCard
                            key={selectedItem?.id}
                            id={selectedItem?.id}
                            title={selectedItem.title}
                            overview={selectedItem?.extended_overview[t("language")] || selectedItem?.extended_overview['en']}
                            backgroundImage={selectedItem.backgroundImage}
                            frontImage={selectedItem.frontImage}
                            technologies={selectedItem.technologies}
                            handleClose={() => setSelectedItem(null)}
                        />
                    )}
            </AnimatePresence>
        </AnimateSharedLayout>
    );
};

const useStyles = makeStyles((theme) => ({
    galleryContainer: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        overflow: "visible",
        width: "100%",
        margin: "0 auto",
        gap: 10,
        flexWrap: 'wrap'
    },
    item: {
        overflow: "visible",
    },
}));

export default ProjectsGallery;
