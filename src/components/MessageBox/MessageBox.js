import React, { useState } from "react";
import { Box, Button, Container, makeStyles, TextField, Typography, useMediaQuery, useTheme } from "@material-ui/core";
import { useFormik } from "formik";
import * as Yup from "yup";
import { AnimatePresence, motion } from "framer-motion";
import Check from "../../assets/images/Check";
import { useTranslation } from "react-i18next"
import { saveProspect } from "../../services/ProspectService";

const MessageBox = () => {
    const theme = useTheme();
    const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
    const classes = useStyles({ isMobile })();
    const [sending, setSending] = useState(false);
    const [sendEmailSuccess, setSendEmailSuccess] = useState(false);
    const [sendEmailMessage, setSendEmailMessage] = useState('')
    const { t } = useTranslation();
    const title = t("contact_title");
    const desc = t("contact_desc");
    const template = t("contact_href");
    const SendEmail = (prospect) => {
        prospect = {
            ...prospect,
            'date': new Date()
        }
        saveProspect(prospect).then(
            (result) => {
                setSendEmailMessage(t('contact_success'))
                setSendEmailSuccess(true);
                setSending(false);
            }
        ).catch(error => {
            setSendEmailMessage(t('contact_error'))
            setSendEmailSuccess(false);
        });
    };
    const formik = useFormik({
        initialValues: {
            name: "",
            email: "",
            message: "",
        },
        validationSchema: Yup.object({
            name: Yup.string().required(t("req_full_name")),
            email: Yup.string().email(t("inv_email")).required(t("req_email")),
            message: Yup.string().required(t("req_message")),
        }),
        onSubmit: (values) => {
            SendEmail(values);
            setSendEmailSuccess(true);
            const message = values.message.toLowerCase();
            const name = values.name;
            window.location.href = eval('`https://api.whatsapp.com/send?phone=51933812390&text=' + template + '`');
        },
        validateOnChange: false,
        validateOnBlur: false,
    });

    return (
        <Container className={classes.container}>

            <Box className={classes.leftSide}>
                <Typography color='white' variant={isMobile ? "h2" : "h1"} className={classes.contact_title}>
                    {title}
                </Typography>
                <Typography color='white' variant={isMobile ? "h5" : "h3"} className={classes.contact_desc}>
                    {desc}
                </Typography>
            </Box>
            <Box overflow="hidden" className={classes.rightSide}>
                <AnimatePresence>
                    {!sendEmailSuccess && (
                        <form className={classes.form} onSubmit={formik.handleSubmit}>
                            <TextField
                                error={Boolean(formik.touched.name && formik.errors.name)}
                                onBlur={formik.handleBlur}
                                onChange={formik.handleChange}
                                value={formik.values.name}
                                helperText={formik.touched.name && formik.errors.name}
                                variant="outlined"
                                margin="normal"
                                type="text"
                                fullWidth
                                id="name"
                                label={t('contact_full_name')}
                                name="name"
                                className={classes.textField}
                                InputProps={{
                                    className: classes.input,
                                }}
                            />
                            <TextField
                                error={Boolean(formik.touched.email && formik.errors.email)}
                                onBlur={formik.handleBlur}
                                onChange={formik.handleChange}
                                value={formik.values.email}
                                helperText={formik.touched.email && formik.errors.email}
                                variant="outlined"
                                type="email"
                                margin="normal"
                                fullWidth
                                id="email"
                                label={t('contact_email')}
                                name="email"
                                className={classes.textField}
                                InputProps={{
                                    className: classes.input,
                                }}
                            />
                            <TextField
                                error={Boolean(formik.touched.message && formik.errors.message)}
                                onBlur={formik.handleBlur}
                                onChange={formik.handleChange}
                                value={formik.values.message}
                                helperText={formik.touched.message && formik.errors.message}
                                variant="outlined"
                                margin="normal"
                                fullWidth
                                name="message"
                                label={t('contact_message')}
                                type="text"
                                id="message"
                                multiline
                                minRows={5}
                                className={classes.textField}
                                InputProps={{
                                    className: classes.input,
                                }}
                            />
                            <Box display="flex" justifyContent={isMobile ? "center" : "left"} mt={2}>
                                <Button
                                    className={classes.submitBtn}
                                    type="submit"
                                    fullWidth
                                    variant="contained"
                                    color="inherit"
                                    disabled={sending}
                                >
                                    {t('contact_btn')}
                                </Button>
                            </Box>
                        </form>
                    )}
                </AnimatePresence>
                <AnimatePresence>
                    {sendEmailSuccess && (
                        <Box
                            component={motion.div}
                            initial={{ opacity: 0 }}
                            animate={{ opacity: 1 }}
                            transition={{ delay: 0.9 }}
                            style={{
                                position: "absolute",
                                top: 0,
                                height: "100%",
                                width: "100%",
                                display: "flex",
                                flexDirection: "column",
                                alignItems: "center",
                                justifyContent: "center",
                            }}
                        >
                            <Box m={2}>
                                <Check style={{ fontSize: 150 }} />
                            </Box>
                            <Typography
                                component={motion.h4}
                                initial={{ opacity: 0, y: 10 }}
                                animate={{ opacity: 1, y: 0 }}
                                transition={{ delay: 1.5, duration: 1 }}
                                variant="body2"
                                style={{ color: 'white' , textAlign:isMobile?'center':'left'}}
                            >
                                {sendEmailMessage}
                            </Typography>
                        </Box>
                    )}
                </AnimatePresence>
            </Box>
        </Container>
    );
};

const useStyles = props => makeStyles((theme) => ({
    container: {
        display: 'flex',
        flexDirection: props.isMobile ? 'column' : 'row',
        alignItems: props.isMobile ? 'center' : "top",
        width: '90%',
    },
    leftSide: {
        width: props.isMobile ? '100%' : '60%'
    },
    rightSide: {
        position: "relative",
        minHeight: "300px",
        width: props.isMobile ? '100%' : '40%',
    },
    form: {
        width: "100%", // Fix IE 11 issue.
        display: 'flex',
        flexDirection: 'column',
        flexWrap: 'wrap',
        alignItems: 'left'
    },
    submitBtn: {
        width: "200px",
        borderRadius: 100,

    },
    contact_title: {
        marginBottom: "30px",
        textAlign: props.isMobile ? 'center' : 'left',
        maxWidth: 450,
        color: theme.palette.white.main
    },
    contact_desc: {
        marginBottom: "15px",
        textAlign: props.isMobile ? 'center' : 'left',
        maxWidth: 500,
        color: theme.palette.white.main
    },
    projects_p: {
        fontSize: theme.typography.bodyLarge.fontSize,
        marginBottom: "15px",
        maxWidth: 800,
        fontWeight: 'light',
        paddingBottom: 25

    },
    textField: {
        '& .MuiInputLabel-root': {
            color: theme.palette.primary.contrastText,
        },
        '& label.Mui-focused': {
            color: theme.palette.primary.contrastText,
        },
        '& .MuiOutlinedInput-root': {
            '& fieldset': {
                borderColor: 'white',
            },
            '&:hover fieldset': {
                borderColor: 'white',
            },
            '&.Mui-focused fieldset': {
                borderColor: '#CDC9FF'
            },
        },
    },
    input: {
        color: theme.palette.primary.contrastText,
    }
}));

export default MessageBox;
