import { createSlice } from '@reduxjs/toolkit'

export const socialSlice = createSlice(
    {
        name: 'socials',
        initialState: {
            'data':[]
        },
        reducers: {
            setSocials: (state, action) => {
                const { payload } = action
                console.log('PAYLOAD',payload)
                state['data'] = payload;
            }
        }
    }
)
export const { setSocials } = socialSlice.actions
export default socialSlice.reducer