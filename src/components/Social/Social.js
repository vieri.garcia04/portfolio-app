import React, { useContext, useEffect } from "react";
import { makeStyles } from "@material-ui/core";
import { motion, useAnimation } from "framer-motion";
import { LinkedIn, Instagram, GitHub, Email, YouTube, Facebook, Twitter, Language, WhatsApp } from "@material-ui/icons";
import IconBtn from "../IconBtn";
// import DarkModeSwitcher from "../DarkModeSwitcher";
import loaderContext from "../../contexts/loaderContext";
import { useSelector } from 'react-redux'

const Social = ({ mobile }) => {
    const classes = useStyles();
    const socialList = useSelector(state => state.socials.data)
    const { isLoading } = useContext(loaderContext);
    const controls = useAnimation();

    useEffect(() => {
        if (!isLoading) {
            controls.start(i => ({
                opacity: 1,
                y: 0,
                transition: {
                    delay: 1.8 + (i * 0.1),
                },
            }));
        } else {
            controls.start({ opacity: 0, y: 0 });
        }
    }, [isLoading, controls]);

    const getSocialIcon = (social) => {
        switch (social.social) {
            case 'GitHub':
                return GitHub;
            case 'Instagram':
                return Instagram;
            case 'LinkedIn':
                return LinkedIn;
            case 'Email':
                return Email;
            case 'YouTube':
                return YouTube;
            case 'Facebook':
                return Facebook;
            case 'Twitter':
                return Twitter;
            case 'WhatsApp':
                return WhatsApp;
            default:
                return Language;
                break;
        }
    }

    if (mobile) {
        return (
            <div className={classes.mobileWrapper}>
                {socialList.map(social => {
                    const icon = getSocialIcon(social);
                    return (
                        <IconBtn key={social.social} icon={icon} m={1} href={social.social_link} />
                    )
                })}
            </div>
        );
    } else {
        return (
            <motion.div className={classes.wrapper}>
                {socialList.map(social => {
                    const icon = getSocialIcon(social);
                    return (
                        <motion.div key={social.social} animate={controls} custom={0}>
                            <IconBtn key={social.social} icon={icon} m={1} href={social.social_link} />
                        </motion.div>
                    )
                })}
            </motion.div>
        );
    }
};

const useStyles = makeStyles((theme) => ({
    wrapper: {
        display: "flex",
        flexDirection: "column",
        alignItems: "flex-end",
        position: "fixed",
        bottom: 0,
        right: 0,
        padding: theme.spacing(2),
        zIndex: 100,
    },
    mobileWrapper: {
        display: "flex",
    },
}));

export default Social;
