import { Avatar, Box, Container, Typography, makeStyles } from '@material-ui/core'
import React from 'react'
import FormatQuoteIcon from '@mui/icons-material/FormatQuote';

const QuoteCard = ({ avatar, title, subtitle, description, key, backgroundColor, containerStyle, iconStyle, titleStyle, descStyle }) => {
    const classes = useStyles({ backgroundColor, containerStyle, titleStyle, descStyle, iconStyle })();
    return (
        <Box>
            <Box
                className={classes.quoteIcon}
            >
                <FormatQuoteIcon color='primary' sx={{ fontSize: 80 }} />
            </Box>
            <Container key={key} className={classes.body}>
                <Box sx={{ width: '30%', display: 'flex', justifyContent: 'center' }}>
                    <Avatar
                        src={avatar}
                        style={{ width: '96px', height: '96px' }}
                        variant="circle"
                    />
                </Box>
                <Box sx={{ width: '70%', paddingY: 2, display: 'flex', flexDirection: 'column' }}>
                    <Typography className={classes.description} variant="body2">
                        {description}
                    </Typography>
                    <Typography className={classes.title} variant="h6" color='inherit' align="center">
                        {title}
                    </Typography>
                    <Typography className={classes.title} variant="subtitle2" color='primary' align="center">
                        {subtitle}
                    </Typography>
                </Box>

            </Container>
        </Box>

    )
}

const useStyles = props => makeStyles((theme) => ({
    quoteIcon: {
        position: 'relative',
        left: '-50%',
        top: `40px`,
        transform: 'rotateX(180deg) rotateY(180deg)',
        zIndex: 5,
        height:30,
    },
    body: {
        backgroundColor: props.backgroundColor,
        display: 'flex',
        flexDirection: 'row',
        width: 450,
        height: 300,
        padding: 10,
        gap: 15,
        border: '5px solid',
        borderColor: '#d1dbfc',
        borderRadius:8,
        //boxShadow: '0px 4px 30px rgba(0, 0, 0, 0.25)',
        [theme.breakpoints.down("sm")]: {
            width: '100%',
            height:285,
            //height: 'auto'
        },
        ...props.containerStyle

    },
    title: {
        textAlign: 'left',
        width: '100%',
        ...props.titleStyle
    },
    description: {
        textAlign: 'left',
        width: '100%',
        flex: 1,
        ...props.descStyle
    }
}))

export default QuoteCard