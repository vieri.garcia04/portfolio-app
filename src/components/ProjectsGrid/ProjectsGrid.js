import React from "react";
import { AnimateSharedLayout } from "framer-motion";
import { Grid, makeStyles } from "@material-ui/core";
import ProjectGridItem from "./ProjectGridItem";
import { projectImagesList } from "../../data";

const ProjectsGrid = () => {

    return (
        <AnimateSharedLayout type="crossfade">
            <Grid container spacing={0} xs={{ display: 'flex' }}>
                {
                    projectImagesList.map((item, k) => (
                        <Grid
                            item
                            xs={12}
                            md={6}
                            key={k}
                        >
                            <ProjectGridItem
                                id={k}
                                projectData={item}
                                image={item.image}
                                initial={{ opacity: 0, y: -50 }}
                                animate={{ opacity: 1, y: 0 }}
                            />
                        </Grid>
                    ))
                }
            </Grid>
        </AnimateSharedLayout>
    );
};

export default ProjectsGrid;
