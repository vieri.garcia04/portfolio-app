import React from "react";
import {
    makeStyles,
    Card as MuiCard,
    CardMedia,
    Typography,
    Box,
    useMediaQuery,
    useTheme,
} from "@material-ui/core";
import { motion, useAnimation } from "framer-motion";
import { useTranslation } from "react-i18next";

const hoverVariants = {
    hover: {
        opacity: 1,
    },
    initial: {
        opacity: 0,
    },
};

const titleVariants = {
    hover: {
        y: 0,
        opacity: 1,
    },
    initial: {
        opacity: 0,
        y: 50,
    },
};

const ProjectGridItem = ({ id, projectData, image, onClick, ...rest }) => {

    const controls = useAnimation();
    const theme = useTheme();
    const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
    const classes = useStyles({ isMobile })();
    const { t } = useTranslation();
    const handleMouseEnterControls = () => {
        controls.start("hover");
    };
    const handleMouseLeaveControls = () => {
        controls.start("initial");
    };
    controls.start("initial");
    return (
        <MuiCard
            className={classes.root}
            component={motion.div}
            layoutId={id}
            onMouseEnter={handleMouseEnterControls}
            onMouseLeave={handleMouseLeaveControls}
            {...rest}
        >
            <div>
                <CardMedia
                    component={motion.div}
                    layoutId={`img-container-${id}`}
                    style={{height:isMobile?270:380}}
                >
                    <motion.img
                        layoutId={`front-img-${id}`}
                        className={classes.frontImage}
                        src={image}
                        alt={projectData.title}
                    />
                </CardMedia>
            </div>
            <motion.div
                transition={{ delay: 0.15 }}
                variants={hoverVariants}
                animate={controls}
                className={classes.hover}
            >
                <Box
                    display="flex"
                    alignItems="center"
                    justofyContent="center"
                    transition={{ delay: 0.5 }}
                    component={motion.div}
                    variants={titleVariants}
                    animate={controls}
                >
                    <Box mr={1} sx={{ textAlign: 'center' }}>
                        <Typography className={classes.view} variant="button">{projectData?.tag ? projectData.tag[t('language')] : 'WEB SITE'}</Typography>
                        <Typography className={classes.view} variant="h4">{projectData.title}</Typography>
                        <Typography className={classes.view} variant="body2">{projectData?.overview ? projectData.overview[t('language')] : ''}</Typography>
                    </Box>
                    {/* <Icon
                        component={motion.div}
                        transition={{ delay: 0.3, repeat: Infinity, duration: 1, repeatType: "reverse" }}
                        variants={{ hover: { y: 7 }, intial: { y: -2 } }}
                        animate="hover"
                        style={{color:'#fff'}}
                    >
                        <ArrowDownward />
                    </Icon> */}
                </Box>
            </motion.div>
        </MuiCard>
    );
};

const useStyles = props => makeStyles((theme) => ({
    view: {
        color: theme.palette.basicColor.white
    },
    root: {
        position: "relative",
        height:  props.isMobile?240:380,
        width: '100%',
        overflow: "hidden",
        cursor: "pointer",
        backgroundColor: theme.palette.primary.main,
        borderRadius: 0
    },
    frontImage: {
        objectFit: props.isMobile?'fit': "cover",
        objectPosition: "center top",
        width: "100%",
        height: "100%",
        boxShadow: theme.shadows[8],
    },
    title: {
        fontSize: "20px",
        fontWeight: 700,
        marginBottom: theme.spacing(1),
        color: theme.palette.secondary.main
    },
    overview: {
        fontSize: "14px",
        marginBottom: theme.spacing(1),
        color: theme.palette.white
    },
    technologies: {
        fontSize: "15px",
        color: theme.palette.primary.contrastText
    },
    hover: {
        position: "absolute",
        top: 0,
        height: "100%",
        width: "100%",
        backgroundColor: "rgba(19,7,72,.95)",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
    },
}));

export default ProjectGridItem;
