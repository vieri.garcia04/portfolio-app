import React, { useContext } from "react";
import {  makeStyles } from "@material-ui/core";
import { Link } from "react-scroll";
import logotipoDark from "../../assets/images/logotipoDark.svg"
// import logotipoLight from "../../assets/images/logotipoLight.svg"
import ThemeContext from "../../contexts/themeContext";

const Logo = ({ setHomeIsActive, ...rest }) => {
    const { isDarkMode } = useContext(ThemeContext);
    const classes = useStyles({isDarkMode})();
    return (
        <Link
            spy
            smooth
            duration={500}
            offset={-70}
            to="home"
            onSetActive={() => setHomeIsActive(true)}
            onSetInactive={() => setHomeIsActive(false)}
            className={classes.root}
        >
           <img src={logotipoDark} alt="logo" {...rest}/>
        </Link>
    );
};

const useStyles =props => makeStyles((theme) => ({
    root: {
        cursor: "pointer"
    },
    logo:{
        backgroundImage:`url(${logotipoDark})`,
    }
}));

export default Logo;
