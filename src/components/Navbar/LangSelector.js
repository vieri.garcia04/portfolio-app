import React, { useState } from "react";
import {useSelector} from 'react-redux'
import { makeStyles, Menu, MenuItem, Button } from "@material-ui/core";
import { KeyboardArrowDownSharp } from "@material-ui/icons";
import i18n from "i18next";
import { useTranslation } from "react-i18next";
import "flag-icon-css/css/flag-icon.min.css";

const LangSelector = (props) => {
    const classes = useStyles();
    const { t } = useTranslation();
    const [anchorEl, setAnchorEl] = useState(null);
    const languages = useSelector(state=> state.languages.data)
    const handleClose = (code) => {
        console.log(code)
        i18n.changeLanguage(code);
        setAnchorEl(null);
        if (props.onClose) {
            props.onClose();
        }
    };

    const currentLanguage = languages.find((elem) => elem.code === t("language"));

    return (
        <div {...props}>
            <Button variant="contained" color="secondary" onClick={(e) => setAnchorEl(e.currentTarget)}>
                {currentLanguage && (
                    <>
                        <span className={`flag-icon flag-icon-${currentLanguage.country_code} ${classes.flagIcon}`} />
                        {currentLanguage.name}
                    </>
                )}
                <KeyboardArrowDownSharp style={{ color: "white" }} />
            </Button>
            <Menu
                id="profile-menu"
                anchorEl={anchorEl}
                open={Boolean(anchorEl)}
                onClose={() => setAnchorEl(null)}
                keepMounted
                elevation={0}
                getContentAnchorEl={null}
                anchorOrigin={{
                    vertical: "bottom",
                    horizontal: "center",
                }}
                transformOrigin={{
                    vertical: "top",
                    horizontal: "center",
                }}
                className={classes.profileMenu}
                disableScrollLock
            >
                {
                    languages.map(language => {
                        return (
                            <MenuItem  key={language.code} className={classes.menuItem} onClick={() => handleClose(language.country_code)}>
                                <span className={`${language.icon} ${classes.flagIcon}`} />
                                {language.name}
                            </MenuItem>
                        )
                    })
                }
            </Menu>
        </div>
    );
};

const useStyles = makeStyles((theme) => ({
    profileMenu: {
        "& .MuiPaper-root": {
            backgroundColor: theme.palette.secondary.main,
            marginTop: "2px",
            boxShadow: theme.shadows[4],
        },
    },
    menuItem: {
        "&:hover": {
            backgroundColor: theme.backgroundSecondary,
        },
    },
    flagIcon: {
        marginRight: theme.spacing(1),
    },
}));

export default LangSelector;
