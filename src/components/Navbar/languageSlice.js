import { createSlice } from '@reduxjs/toolkit'

export const languageSlice = createSlice(
    {
        name: 'languages',
        initialState: {
            'data':[]
        },
        reducers: {
            setLanguages: (state, action) => {
                const { payload } = action
                state['data'] = payload;
            }
        }
    }
)
export const { setLanguages } = languageSlice.actions
export default languageSlice.reducer