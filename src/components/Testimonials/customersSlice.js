import { createSlice } from '@reduxjs/toolkit'



export const customersSlice = createSlice(
    {
        name: 'customers',
        initialState: {
            'data': []
        },
        reducers: {
            setCustomers: (state, action) => {
                let { payload } = action
                state['data'] = payload;
            }
        }
    }
)
export const { setCustomers } = customersSlice.actions
export default customersSlice.reducer