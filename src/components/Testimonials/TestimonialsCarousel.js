import { Typography, makeStyles, useMediaQuery, Container, useTheme, Box } from "@material-ui/core";
import { useTranslation } from "react-i18next";
import QuoteCard from "../QuoteCard/QuoteCard";
import Carousel from "react-material-ui-carousel";
import { partList } from "../../utils/arrayUtils";

const TestimonialsCarousel = ({ customers, isDarkMode }) => {
    const theme = useTheme();
    const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
    const blocks = partList(customers, 3);
    return (
        <Container style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', width: '100%'}}>
            <Carousel animation="slide" sx={{ width: '100%' }}>
                {isMobile ?
                    customers.map((elem, k) => (
                        <TestimonialCarouselItem
                            key={k}
                            index={k}
                            customer={elem}
                        />
                    ))
                    : blocks.map((block, k) => (
                        <Box sx={{ width: '100%', height: 400, display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', gap: '33px' }}>
                            {block.map((elem, k) => (
                                <TestimonialCarouselItem
                                    key={k}
                                    index={k}
                                    customer={elem}
                                />
                            ))}

                        </Box>
                    ))}
            </Carousel>
        </Container>
    )
}


const TestimonialCarouselItem = ({ customer, index }) => {
    
    const { t } = useTranslation();

    return (
        <QuoteCard
            title={`${customer.name} ${customer.lastname}`}
            description={customer.testimonial?.[t('language')]}
            subtitle={customer.position?.[t('language')]}
            avatar={customer.avatar}
            key={index}
            containerStyle={{
                width: 360*1.1,
                height: 280,
                marginX: '5px'
            }}
        // iconStyle={{
        //     '&:hover': {
        //         backgroundImage: `url(${hoverIcon})`
        //     },
        // }}
        // titleStyle={{
        //     color: "#47464f",
        //     textAlign: 'center'
        // }}
        // descStyle={{
        //     color: "#47464f",
        //     textAlign: 'center'
        // }}
        />
    )
}

const useStyles = props => makeStyles((theme) => ({

    customerName: {
        textAlign: 'center',
        width: '100%',
        [theme.breakpoints.down("xs")]: {
            fontSize: theme.typography.h6.fontSize,
        },
    },
    testimonial: {
        textAlign: 'center',
        width: '100%',
        [theme.breakpoints.down("xs")]: {
            fontSize: theme.typography.titleSmall.fontSize,
        },
    },
    customers: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    }
}));


export default TestimonialsCarousel;