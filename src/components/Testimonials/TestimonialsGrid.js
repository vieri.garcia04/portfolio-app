import { Typography, makeStyles, useMediaQuery, Container, useTheme, Box } from "@material-ui/core";
import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import InfoCard from "../InfoCard/InfoCard"

const TestimonialsGrid = ({ customers, isDarkMode }) => {
    const theme = useTheme();
    const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
    const [currentTestimonial, setCurrentTestimonial] = useState({});
    const classes = useStyles({ isMobile, isDarkMode })();
    const { t } = useTranslation();

    const handleCustomerFocus = (companyName, testimonial) => {
        setCurrentTestimonial({
            companyName,
            testimonial
        })
    }
    useEffect(() => {
        setCurrentTestimonial({})
    }, [t('language')])


    return (
        <Box style={{ width: '100%' }}>
            <Container className={classes.container}>
                {customers.map((elem, k) => (
                    <TestimonialGridItem
                        key={k}
                        index={k}
                        customerName={elem.company_name}
                        testimonial={elem.testimonial[t('language')]}
                        icon={elem.icon}
                        hoverIcon={elem?.hoverIcon}
                        isMobile={isMobile}
                        theme={theme}
                        onFocus={handleCustomerFocus}
                    />
                ))}
            </Container>
            {
                !isMobile && (
                    <Box style={{ display: 'flex', flexDirection: 'column', gap: 15, width: '100%', textAlign: 'center', marginTop: 25 }}>
                        <Typography variant="h5">
                            {currentTestimonial['companyName']}
                        </Typography>
                        <Typography variant="bodyLarge">
                            {currentTestimonial['testimonial']}
                        </Typography>
                    </Box>
                )
            }
        </Box>
    )
}


const TestimonialGridItem = ({ icon, hoverIcon, customerName, testimonial, index, isMobile, theme, onFocus }) => {

    let borders = {}
    if (!isMobile) {
        borders = {
            borderBottomStyle: [1, 2].includes(index + 1) ? 'solid' : 'none',
            borderTopStyle: 'none',
            borderLeftStyle: 'none',
            borderRightStyle: (index + 1) % 2 === 1 ? 'solid' : 'none',

        }
    } else {
        borders = {
            borderBottomStyle: 'solid',
        }
    }
    return (
        <InfoCard
            title={customerName}
            description={testimonial}
            icon={icon}
            key={index}
            onMouseOver={() => onFocus(customerName, testimonial)}
            containerStyle={{
                boxShadow: '0px 0px 0px rgba(0, 0, 0, 0.25)',
                justifyContent: isMobile ? 'center' : 'top',
                backgroundColor: 'white',
                height: 'auto',
                width: isMobile ? '100%' : `49%`,
                padding: 50,
                borderRadius: 0,
                alignItems: 'center',
                spaceBettween: 10,
                margin: 0,
                marginTop: 0,
                ...borders,
                borderColor: "#e4e1ec",
                [theme.breakpoints.down("sm")]: {
                    alignItems: 'center',
                    '&:hover': {
                        width: '100%'
                    },
                },
                [theme.breakpoints.down("xs")]: {
                    '&:hover': {
                        width: '100%'
                    },
                }
            }}
            iconStyle={{
                '&:hover': {
                    backgroundImage: `url(${hoverIcon})`
                },
            }}
            titleStyle={{
                color: "#47464f",
                display: isMobile ? '' : 'none',
                textAlign: 'center'
            }}
            descStyle={{
                color: "#47464f",
                display: isMobile ? '' : 'none',
                textAlign: 'center'
            }}
        />
    )
}

const useStyles = props => makeStyles((theme) => ({
    container: {
        width: props.isMobile ? '100%' : '62%',
        height: 'auto',
        display: 'flex',
        flexDirection: props.isMobile ? 'column' : 'row',
        alignItems: props.isMobile ? 'center' : 'flex-start',
        justifyContent: 'center',
        flexWrap: 'wrap',
        gap: props.isMobile ? 5 : 0,
        padding: 0
    },
    customerName: {
        textAlign: 'center',
        width: '100%',
        [theme.breakpoints.down("xs")]: {
            fontSize: theme.typography.h6.fontSize,
        },
    },
    testimonial: {
        textAlign: 'center',
        width: '100%',
        [theme.breakpoints.down("xs")]: {
            fontSize: theme.typography.titleSmall.fontSize,
        },
    },
    customers: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    }
}));


export default TestimonialsGrid;