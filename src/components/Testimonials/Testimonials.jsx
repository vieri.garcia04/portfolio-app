import { useSelector } from 'react-redux'
import TestimonialsCarousel from "./TestimonialsCarousel";


const Testimonials = ({ animate, isDarkMode }) => {
  const customers = useSelector(state => state.customers.data)


  return (
    <TestimonialsCarousel
      customers={customers}
    />
  );
};



export default Testimonials;
