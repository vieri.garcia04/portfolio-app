import React from "react";
import LoaderContainer from "../../containers/LoaderContainer";
import VG from "../../assets/images/VG";

const Loader = () => {
    return (
        <LoaderContainer>
            <VG width={250} />
        </LoaderContainer>
    );
};

export default Loader;
