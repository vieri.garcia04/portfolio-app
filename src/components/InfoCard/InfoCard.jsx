import { Container, Typography, makeStyles, useMediaQuery, useTheme, } from '@material-ui/core'
import React from 'react'

const InfoCard = ({ icon, title, description, key, backgroundColor, containerStyle,onMouseOver, iconStyle, titleStyle, descStyle }) => {
    const theme = useTheme();
    const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
    const classes = useStyles({ isMobile, backgroundColor, icon, containerStyle, titleStyle, descStyle, iconStyle })();
    return (
        <Container key={key} className={classes.reason}    >
            <div className={classes.icon} onMouseOver={onMouseOver} />
            <Typography className={classes.title} variant="h5" color='primary' align="center">
                {title}
            </Typography>
            <Typography className={classes.description} variant="titleMedium">
                {description}
            </Typography>
        </Container>
    )
}

const useStyles = props => makeStyles((theme) => ({
    reason: {
        backgroundColor: props.backgroundColor,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 12,
        flexWrap: 'wrap',
        width: 400,
        height: 300,
        padding: 5,
        marginTop: 10,
        gap: 15,
        boxShadow: '0px 4px 30px rgba(0, 0, 0, 0.25)',
        [theme.breakpoints.down("sm")]: {
            width: 400,
            '&:hover': {
                width: 430,
                height: 330,
            },
        },
        [theme.breakpoints.down("xs")]: {
            width: 350,
            '&:hover': {
                width: 370,
                height: 330,
            },
        },
        ...props.containerStyle

    },
    title: {
        textAlign: 'center',
        width: '100%',
        [theme.breakpoints.down("xs")]: {
            fontSize: theme.typography.h6.fontSize,
        },
        ...props.titleStyle
    },
    description: {
        textAlign: 'center',
        width: '100%',
        [theme.breakpoints.down("xs")]: {
            fontSize: theme.typography.titleSmall.fontSize,
        },
        ...props.descStyle
    },
    icon: {
        backgroundImage:`url(${props.icon})`,
        width:96,
        height:96,
        ...props.iconStyle
    }
}))

export default InfoCard