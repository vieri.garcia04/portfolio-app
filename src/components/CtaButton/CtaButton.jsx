import { Box, Button, makeStyles, useMediaQuery, useTheme } from '@material-ui/core'
import React from 'react'
import { Link } from 'react-scroll'

const CtaButton = ({ ctaTitle, fixed }) => {
    const theme = useTheme();
    const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
    const classes = useStyles({ isMobile, fixed})();
    return (
        <Button
            component={Link}
            spy
            smooth
            offset={0}
            duration={500}
            to="contact"
            variant="contained"
            size="large"
            color='primary'
            className={classes.ctaButton}
        >
            <Box fontWeight="fontWeightRegular">
                {ctaTitle}
            </Box>
        </Button>
    )
}
const useStyles = props => makeStyles((theme) => ({

    ctaButton: {
        fontWeight: 'bold',
        position: props.fixed ? 'fixed':'relative',
        bottom: 0,
        marginBottom: theme.spacing(5),
        borderRadius: 100,
    }

}));
export default CtaButton