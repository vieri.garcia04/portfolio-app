import { createSlice } from '@reduxjs/toolkit'



export const reasonSlice = createSlice(
    {
        name: 'reasons',
        initialState: {
            'data': []
        },
        reducers: {
            setReasons: (state, action) => {
                const { payload } = action
                state['data'] = payload;
            }
        }
    }
)
export const { setReasons } = reasonSlice.actions
export default reasonSlice.reducer