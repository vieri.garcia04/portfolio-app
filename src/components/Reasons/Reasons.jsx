import { makeStyles, useMediaQuery, Container, useTheme, Box } from "@material-ui/core";
// import { motion } from "framer-motion";
import { useTranslation } from "react-i18next";
import Carousel from "react-material-ui-carousel";
import { useSelector } from 'react-redux'
import { useEffect, useState } from "react";
import { partList } from "../../utils/arrayUtils"
import InfoCard from "../InfoCard/InfoCard"


const Reasons = ({ animate, isDarkMode }) => {
    const theme = useTheme();
    const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
    const [carouselPages, setCarouselPages] = useState([])
    const reasons = useSelector(state => state.reasons.data)
    const classes = useStyles({ isMobile, isDarkMode })();
    const { t } = useTranslation();

    useEffect(() => {
        setCarouselPages(partList(reasons, 3))
    }, [reasons])


    return (
        <Container className={classes.container}>
            {
                isMobile ?
                    reasons.map((elem, k) => (
                        <Reason
                            key={k}
                            reasonTitle={elem.title[t('language')]}
                            reasonDescription={elem.description[t('language')]}
                            reasonIcon={elem.icon}
                        />
                    ))
                    :
                    <Carousel animation="slide" duration={500} sx={{paddingBottom:50}}>
                        {carouselPages && carouselPages.map((pageReasons, i) => (
                            <Box className={classes.reasons} key={i}>
                                {pageReasons.map((elem, k) => (
                                    <Reason
                                        key={k}
                                        reasonTitle={elem.title[t('language')]}
                                        reasonDescription={elem.description[t('language')]}
                                        reasonIcon={elem.icon}
                                    />
                                ))}
                            </Box>
                        ))}
                    </Carousel>
            }

        </Container>

    );
};

const Reason = ({ reasonIcon, reasonTitle, reasonDescription, key }) => {
    return (
        <InfoCard
            title={reasonTitle}
            description={reasonDescription}
            icon={reasonIcon}
            key={key}
        />
    )
}
const useStyles = props => makeStyles((theme) => ({
    container: {
        width: '100%',
        height:props.isMobile?'auto':300,
        display: 'flex',
        flexDirection: 'column',
        alignItems: props.isMobile?'center':'none',
        gap:15,
        padding: 0
    },
    reason: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 12,
        flexWrap: 'wrap',
        width: 400,
        height: 300,
        padding: 5,
        marginTop: 20,
        gap: 15,
        boxShadow: '0px 4px 30px rgba(0, 0, 0, 0.25)',
        [theme.breakpoints.down("sm")]: {
            width: 400,
        },
        [theme.breakpoints.down("xs")]: {
            width: 350
        },
        '&:hover': {
            width: 430,
            height: 330,
         },

    },
    reasonTitle: {
        textAlign: 'center',
        width: '100%',
        [theme.breakpoints.down("xs")]: {
            fontSize: theme.typography.h6.fontSize,
        },
    },
    reasonDescription: {
        textAlign: 'center',
        width: '100%',
        [theme.breakpoints.down("xs")]: {
            fontSize: theme.typography.titleSmall.fontSize,
        },
    },
    reasons: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    }
}));

export default Reasons;
