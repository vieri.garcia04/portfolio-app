import { makeStyles, useMediaQuery, Container, useTheme, Box } from "@material-ui/core";
import { useTranslation } from "react-i18next";
import Carousel from "react-material-ui-carousel";
import { useSelector } from 'react-redux'
import { useEffect, useState } from "react";
import { partList } from "../../utils/arrayUtils"
import InfoCard from "../InfoCard/InfoCard"


const Services = ({ animate, isDarkMode }) => {
    const theme = useTheme();
    const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
    const [carouselPages, setCarouselPages] = useState([])
    let services = useSelector(state => state.services.data)
    const classes = useStyles({ isMobile, isDarkMode })();
    const { t } = useTranslation();

    useEffect(() => {
        setCarouselPages(partList(services, 3))
    }, [services])


    return (
        <Container className={classes.container}>
            {
                isMobile ?
                    services.map((elem, k) => (
                        <Service
                            key={k}
                            offerServiceTitle={elem.title[t('language')]}
                            offerServiceDescription={elem.description[t('language')]}
                            offerServiceIcon={elem.icon}
                        />
                    ))
                    :
                    <Carousel animation="slide" duration={500} sx={{paddingBottom:50}}>
                        {carouselPages && carouselPages.map((pageServices, i) => (
                            <Box className={classes.services} key={i}>
                                {pageServices.map((elem, k) => (
                                    <Service
                                        key={k}
                                        offerServiceTitle={elem.title[t('language')]}
                                        offerServiceDescription={elem.description[t('language')]}
                                        offerServiceIcon={elem.icon}
                                    />
                                ))}
                            </Box>
                        ))}
                    </Carousel>
            }

        </Container>

    );
};

const Service = ({ offerServiceIcon, offerServiceTitle, offerServiceDescription, key }) => {
    const theme=useTheme()
    return (
        <InfoCard
            title={offerServiceTitle}
            description={offerServiceDescription}
            icon={offerServiceIcon}
            key={key}
            backgroundColor="white"
            containerStyle={{
                [theme.breakpoints.down("sm")]: {
                    width: '100%',
                    '&:hover': {
                        width:'100%',
                        height: 330,
                    },
                },
            }}
        />
    )
}
const useStyles = props => makeStyles((theme) => ({
    container: {
        width: '100%',
        height:props.isMobile?'auto':300,
        display: 'flex',
        flexDirection: 'column',
        flexWrap:'wrap',
        alignItems: props.isMobile?'center':'none',
        gap:15,
        padding: 0
    },
    offerService: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 12,
        flexWrap: 'wrap',
        width: 400,
        height: 300,
        padding: 5,
        marginTop: 20,
        gap: 15,
        boxShadow: '0px 4px 30px rgba(0, 0, 0, 0.25)',
        [theme.breakpoints.down("sm")]: {
            width: 400,
        },
        [theme.breakpoints.down("xs")]: {
            width: 350
        },
        '&:hover': {
            width: 430,
            height: 330,
         },

    },
    offerServiceTitle: {
        textAlign: 'center',
        width: '100%',
        [theme.breakpoints.down("xs")]: {
            fontSize: theme.typography.h6.fontSize,
        },
    },
    offerServiceDescription: {
        textAlign: 'center',
        width: '100%',
        [theme.breakpoints.down("xs")]: {
            fontSize: theme.typography.titleSmall.fontSize,
        },
    },
    services: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    }
}));

export default Services;
