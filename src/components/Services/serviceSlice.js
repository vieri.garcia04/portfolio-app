import { createSlice } from '@reduxjs/toolkit'
import { sortArray } from '../../utils/arrayUtils';



export const serviceSlice = createSlice(
    {
        name: 'services',
        initialState: {
            'data': []
        },
        reducers: {
            setServices: (state, action) => {
                const { payload } = action
                state['data'] = sortArray(payload,'order');
            }
        }
    }
)
export const { setServices } = serviceSlice.actions
export default serviceSlice.reducer