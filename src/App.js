import { useState, useEffect, Suspense } from "react";
import { useDispatch } from 'react-redux'
import { initReactI18next } from "react-i18next";
import LanguageDetector from "i18next-browser-languagedetector";
import { ThemeProvider } from "@material-ui/styles";
import { darkTheme, lightTheme } from "./assets/theme";
import CssBaseline from "@material-ui/core/CssBaseline";
import "./App.css";
import ScrollToTop from "./components/ScrollToTop";
import { BrowserRouter as Router } from "react-router-dom";
import Routes from "./Routes";
import ThemeContext from "./contexts/themeContext";
import LoaderContext from "./contexts/loaderContext";

import { setProcess } from './components/Process/processSlice'
import { getProcesss } from "./services/ProcessService";

import { setReasons } from './components/Reasons/reasonSlice'
import { getReasons } from "./services/ReasonService";

import { setCustomers } from './components/Testimonials/customersSlice'
import { getCustomers } from "./services/CustomerService";

import { setCounters } from './features/site/sections/About/counterSlice'
import { getCounters } from "./services/CounterService";

import { setServices } from './components/Services/serviceSlice'
import { getServices } from "./services/ServiceService";

// import { setBenefits } from './components/Benefits/benefitSlice'
// import { getBenefits } from "./services/BenefitService";

import { getSocials } from './services/SocialService'
import { setSocials } from './components/Social/socialSlice'

import { setLanguages } from './components/Navbar/languageSlice'
import { getLanguages } from './services/LanguagesService'

import i18n from "./i18n";
import { getLabels } from "./services/LabelsService";
import { getSkills } from "./services/SkillService";
import { setSkills } from './components/Skills/skillSlice'


function App() {
    const [isDarkMode, setIsDarkMode] = useState(false);
    const [isLoading, setIsLoading] = useState(true);
    const dispatch = useDispatch();

    useEffect(() => {
        loadData();
        if (window.matchMedia && window.matchMedia("(prefers-color-scheme: dark)").matches) {
            setIsDarkMode(true);
        } else {
            setIsDarkMode(false);
        }
    }, []);

    const loadData = async () => {

        // Load reasons
        let querySnapshot = await getReasons()
        const reasons = [];
        querySnapshot.forEach((doc) => {
            reasons.push({ ...doc.data(), id: doc.id });
        });
        dispatch(setReasons(reasons));
         // Load services
         querySnapshot = await getProcesss()
         const process = [];
         querySnapshot.forEach((doc) => {
            process.push({ ...doc.data(), id: doc.id });
         });
         dispatch(setProcess(process));
          // Load customers
          querySnapshot = await getCustomers()
          const customers = [];
          querySnapshot.forEach((doc) => {
            customers.push({ ...doc.data(), id: doc.id });
          });
          dispatch(setCustomers(customers));
        // Load services
        querySnapshot = await getServices()
        const services = [];
        querySnapshot.forEach((doc) => {
            services.push({ ...doc.data(), id: doc.id });
        });
        dispatch(setServices(services));
        // Load counters
        querySnapshot = await getCounters()
        const counters = [];
        querySnapshot.forEach((doc) => {
            counters.push({ ...doc.data(), id: doc.id });
        });
        dispatch(setCounters(counters));
        // Load Skills
        querySnapshot = await getSkills()
        const skills = [];
        querySnapshot.forEach((doc) => {
            skills.push({ ...doc.data(), id: doc.id });
        });
        dispatch(setSkills(skills));
        // Load Socials
        querySnapshot = await getSocials()
        const socials = [];
        querySnapshot.forEach((doc) => {
            socials.push({ ...doc.data(), id: doc.id });
        });
        dispatch(setSocials(socials));
        // Load languages
        querySnapshot = await getLanguages()
        const languages = [];
        querySnapshot.forEach((doc) => {
            languages.push({ ...doc.data(), id: doc.id });
        });
        dispatch(setLanguages(languages));
        // Load Labels
        querySnapshot = await getLabels()
        let labels = {};
        querySnapshot.forEach((doc) => {
            labels = { ...doc.data(), id: doc.id };
        });
        let res = {}
        Object.keys(labels).forEach(lan => {
            res[lan] = {
                translation: { language: lan, ...labels[lan] }
            }
        })
        console.log(res)
        // Initialize language labels
        i18n.use(LanguageDetector)
            .use(initReactI18next)
            .init({
                resources: res,
                fallbackLng: "en",
                detection: {
                    order: ["cookie", "localStorage", "navigator", "htmlTag"],
                    caches: ["cookie"],
                }
            })

    }


    return (
        <Router>
            <Suspense fallback={<div></div>}>
                <ThemeContext.Provider value={{ isDarkMode, setIsDarkMode }}>
                    <LoaderContext.Provider value={{ isLoading, setIsLoading }}>
                        <ThemeProvider theme={isDarkMode ? darkTheme : lightTheme}>
                            <CssBaseline />
                            <ScrollToTop />
                            <Routes />
                        </ThemeProvider>
                    </LoaderContext.Provider>
                </ThemeContext.Provider>
            </Suspense>
        </Router>
    );
}

export default App;
