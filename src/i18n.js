import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import LanguageDetector from "i18next-browser-languagedetector";
import en from "./assets/translations/en.json";
import es from './assets/translations/es.json';
// import {skillsList, reasonsList,servicesList, counterLits, process, customersList} from "./data"
// import {saveLabel} from '../src/services/LabelsService'
// import {saveSkill} from '../src/services/SkillService'
// import {saveReason} from '../src/services/ReasonService'
// import {saveService} from '../src/services/ServiceService'
// import {saveCounter} from '../src/services/CounterService'
// import {saveProcess} from '../src/services/ProcessService'
// import {saveCustomer} from '../src/services/CustomerService'

const resources = {
    en: {
        translation: en,
    },
    es: {
        translation: es,
    },
};

// counterLits.forEach(item=>{
//     saveCounter(item)
// })
// servicesList.forEach(item=>{
//     saveService(item)
// })
// reasonsList.forEach(item=>{
//     saveReason(item)
// })
// process.forEach(item=>{
//     saveProcess(item)
// })
// customersList.forEach(item=>{
//     saveCustomer(item)
// })
// saveLabel({en, es})
i18n.use(LanguageDetector)
    .use(initReactI18next)
    .init({
        resources,
        fallbackLng: "en",
        detection: {
            order: ["cookie", "localStorage", "navigator", "htmlTag"],
            caches: ["cookie"],
        },
        // interpolation: {
        //     escapeValue: false,
        // },
    });

export default i18n;
