import React from "react";
import SectionContainer from "../../../../containers/SectionContainer";
import { useTranslation } from "react-i18next";
import { Container, Typography, makeStyles, Box } from "@material-ui/core";
import ProjectsGrid from "../../../../components/ProjectsGrid/ProjectsGrid";

const Projects = () => {
  const classes = useStyles();
  const { t } = useTranslation();
  const title = t("projects_title");
  const desc = t("projects_desc");

  return (
    <SectionContainer id="projects"  paddingHorizontal={0}  >
      <Container className={classes.container}>
        <Box className={classes.questionContainer}>
          <Typography
            custom={2}
            variant="h2"
            color='primary'
            className={classes.question}
          >
            {title}
          </Typography>
          <Typography
            custom={2}
            variant="body2"
            className={classes.answer}
          >
            {desc}
          </Typography>
        </Box>

      </Container>
      <ProjectsGrid />

    </SectionContainer>
  );
};

const useStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    gap: 15,
    height: 'auto',
    paddingLeft: 0,
    paddingRight: 0,
    marginBottom: 50
  },
  questionContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    flexWrap: 'wrap',
    gap: 20,
    textAlign: 'center',
    paddingLeft: 15,
    paddingRight: 15
  },
  question: {
    [theme.breakpoints.down("sm")]: {
      fontSize: theme.typography.h2.fontSize,
    },
  },
  answer: {
    maxWidth: 900,
    marginBottom: 30,
    [theme.breakpoints.down("sm")]: {
      fontSize: theme.typography.h6.fontSize,
    },
  },
  projects_p: {
    fontSize: theme.typography.bodyLarge.fontSize,
    marginBottom: "15px",
    maxWidth: 800,
    fontWeight: 'light',
    paddingBottom: 25

  }
}));
export default Projects;
