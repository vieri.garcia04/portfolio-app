import React, { useContext, useEffect, useState } from "react";
import { Typography, Button, makeStyles, useTheme, useMediaQuery, Container } from "@material-ui/core";
import { motion, useAnimation } from "framer-motion";
import { Link } from "react-scroll";
import HomeContainer from "../../../../containers/HomeContainer";
import { useTranslation } from "react-i18next";
import loaderContext from "../../../../contexts/loaderContext";
import { Box } from '@material-ui/core';
import themeContext from "../../../../contexts/themeContext";
import Carousel from 'react-material-ui-carousel'
import CtaButton from "../../../../components/CtaButton/CtaButton"
import { Player } from 'video-react';
import 'video-react/dist/video-react.css'
import { getSourceUrl } from "../../../../utils/storageUtils";
import Reasons from "../../../../components/Reasons/Reasons";

const Home = () => {

    const { isLoading } = useContext(loaderContext);
    const { isDarkMode } = useContext(themeContext);
    const [homeVideo, setHomeVideo] = useState();
    const theme = useTheme();
    const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
    const classes = useStyles({ isDarkMode, isMobile })();

    const controls = useAnimation();
    const { t } = useTranslation();

    useEffect(() => {
        getSourceUrl('videos/home_video.mp4', setHomeVideo);
    }, [])

    useEffect(() => {
        if (!isLoading) {
            controls.start((i) => ({
                y: 0,
                opacity: 1,
                transition: { delay: i * 0.1 + 1.2 },
            }));
        } else {
            controls.start({ opacity: 0, y: 5 });
        }
    }, [isLoading, controls]);

    const homeItems = [
        {
            title: t("home_title"),
            subtitle: t("home_subtitle"),
            description: t("home_description")
        },
        {
            title: t("home_title_2"),
            subtitle: t("home_subtitle_2"),
            description: t("home_description_2")
        }
    ]
    const home_question = t("home_question");
    const home_answer = t("home_answer");
    return (

        <Box sx={{ height: 'auto' }}>
            <HomeContainer id="home">
                <Carousel className={classes.carousel} animation="slide">
                    {
                        homeItems.map((item, i) =>
                            <Box className={classes.container} key={i}>
                                <Box sx={{ display: 'flex', flexDirection: 'column', flexWrap: 'wrap', gap: 2 }}>
                                    <Typography
                                        component={motion.p}
                                        animate={controls}
                                        custom={2}
                                        variant="h1"
                                        className={classes.title}
                                    >
                                        {item.title}
                                    </Typography>
                                    <Typography
                                        component={motion.p}
                                        animate={controls}
                                        custom={2}
                                        variant="h1"
                                        color='primary'
                                        className={classes.subtitle}
                                    >
                                        {item.subtitle}
                                    </Typography>

                                </Box>

                                <Typography
                                    component={motion.p}
                                    animate={controls}
                                    custom={2}
                                    variant="h6"
                                    className={classes.description}
                                >
                                    {item.description}
                                </Typography>
                                <motion.div animate={controls} custom={5}>
                                    <CtaButton
                                        ctaTitle={t("home_cta")}
                                        fixed={true}
                                    />
                                </motion.div>
                            </Box>)
                    }
                </Carousel>
            </HomeContainer>
            <Container className={classes.container2}>
                <Box className={classes.questionContainer}>
                    <Typography
                        custom={2}
                        variant="h2"
                        color='primary'
                        className={classes.question}
                    >
                        {home_question}
                    </Typography>
                    <Typography
                        custom={2}
                        variant="body2"
                        className={classes.answer}
                    >
                        {home_answer}
                    </Typography>

                </Box>
                <Box className={classes.reasonsContainer}>
                    <Reasons />
                </Box>
            </Container>
        </Box>

    );
};

const useStyles = props => makeStyles((theme) => ({
    title: {
        [theme.breakpoints.down("sm")]: {
            fontSize: theme.typography.h2.fontSize,
        },
        color: theme.palette.black
    }, subtitle: {
        marginBottom: props.isMobile ? "0px" : "10px",
        [theme.breakpoints.down("sm")]: {
            fontSize: theme.typography.h2.fontSize,
        },
        color: theme.palette.primary
    }, description: {
        marginBottom: props.isMobile ? "0px" : "10px",
        color: theme.palette.black,
        [theme.breakpoints.down("sm")]: {
            fontSize: theme.typography.bodyLarge.fontSize
        }
    },
    container: {
        backgroundColor: theme.palette.white.main,
        display: 'flex',
        flexDirection: 'column',
        flexWrap: 'wrap',
        width: theme.spacing(50),
        alignItems: 'flex-start',
        gap: theme.spacing(2),
        padding: '45px 35px',
        gap: theme.spacing(1),
        height: 380,
        [theme.breakpoints.down("sm")]: {
            margin: theme.spacing(5),
            width: 'auto',
            height: 370
        },

    },
    carousel: {
        width: theme.spacing(50)
    },
    container2: {
        backgroundColor: theme.palette.white.main,
        display: 'flex',
        flexDirection: 'column',
        width: '100%',
        justifyContent: 'top',
        gap: theme.spacing(2),
        height: 'auto',
        flexGrow: 1,
        paddingLeft: 0,
        paddingRight: 0,
        marginBottom: 50,
        [theme.breakpoints.down("sm")]: {
            width: 'auto',
        },


    },
    questionContainer: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        flexWrap: 'wrap',
        gap: 20,
        textAlign: 'center',
        paddingTop: `calc( ${theme.spacing(2)}px + ${theme.navbarHeight} ) `,
        paddingLeft: 10,
        paddingRight: 10
    },
    question: {
        [theme.breakpoints.down("sm")]: {
            fontSize: theme.typography.h2.fontSize,
        },
    },
    answer: {
        maxWidth: 600,
        [theme.breakpoints.down("sm")]: {
            fontSize: theme.typography.h6.fontSize,
        },
    },
    reasonsContainer: {
        height: 'auto',
        width: '100%',
        marginTop: theme.spacing(5),
        marginBottom: theme.spacing(5)
    },


}));

export default Home;
