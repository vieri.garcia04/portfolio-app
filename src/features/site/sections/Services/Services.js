import React from "react";
import { useTheme, Grid, Typography, makeStyles, Box, useMediaQuery, Container } from "@material-ui/core";
import SectionContainer from "../../../../containers/SectionContainer";
import Skills from "../../../../components/Skills";
import Avatar from "../../../../components/Avatar";
import { useTranslation } from "react-i18next";
import CtaButton from "../../../../components/CtaButton/CtaButton"
import OfferServices from "../../../../components/Services/Services"

const Services = () => {
    const theme = useTheme();
    const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
    const classes = useStyles({ isMobile })();
    const { t } = useTranslation();
    const services_title = t("services_title");
    const services_desc = t("services_desc");
    const services_cta = t("services_cta");
    return (
        <SectionContainer id="services" title={t("menu_services")}
            showTitle={false}
            maxWidth='xl'
            backgroundColor="#CDC9FF" 
            paddingHorizontal={0}
            >
            <Container className={classes.container}>
                <Box className={classes.questionContainer}>
                    <Typography
                        custom={2}
                        variant="h2"
                        color='primary'
                        className={classes.question}
                    >
                        {services_title}
                    </Typography>
                    <Typography
                        custom={2}
                        variant="body2"
                        className={classes.answer}
                    >
                        {services_desc}
                    </Typography>
                    <div>
                        <CtaButton
                            ctaTitle={services_cta}
                        />
                    </div>

                </Box>
                <Box className={classes.reasonsContainer}>
                    <OfferServices/>
                </Box>
            </Container>
        </SectionContainer>
    );
};

const useStyles = props => makeStyles((theme) => ({
    container: {
        display: 'flex',
        flexDirection: 'column',
        width: '100%',
        gap: 15,
        height: 'auto',
        paddingLeft: 0,
        paddingRight: 0,
        marginBottom: 50
    },
    questionContainer: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        flexWrap: 'wrap',
        gap: 20,
        textAlign: 'center',
        paddingLeft:15,
        paddingRight:15
    },
    question: {
        [theme.breakpoints.down("sm")]: {
            fontSize: theme.typography.h2.fontSize,
        },
    },
    answer: {
        maxWidth: 600,
        [theme.breakpoints.down("sm")]: {
            fontSize: theme.typography.h6.fontSize,
        },
    },
    reasonsContainer: {
        height: 'auto',
        width: '100%',
    },
}));

export default Services;
