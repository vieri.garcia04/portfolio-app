import React from "react";
import SectionContainer from "../../../../containers/SectionContainer";
import MessageBox from "../../../../components/MessageBox/MessageBox";
import { useTheme } from "@material-ui/core";

const Contact = () => {
  const theme = useTheme();
  return (
    <SectionContainer id="contact"  
    backgroundColor={theme.palette.primary.main}
    >
      <MessageBox />
    </SectionContainer>
  );
};

export default Contact;
