import { createSlice } from '@reduxjs/toolkit'



export const counterSlice = createSlice(
    {
        name: 'counters',
        initialState: {
            'data': []
        },
        reducers: {
            setCounters: (state, action) => {
                const { payload } = action
                state['data'] = payload;
            }
        }
    }
)
export const { setCounters } = counterSlice.actions
export default counterSlice.reducer