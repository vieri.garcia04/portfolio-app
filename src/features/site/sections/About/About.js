import React, { useEffect, useState } from "react";
import { useTheme, Typography, makeStyles, Box, useMediaQuery } from "@material-ui/core";
import SectionContainer from "../../../../containers/SectionContainer";
import { useTranslation } from "react-i18next";
import { useSelector } from 'react-redux'
import CtaButton from "../../../../components/CtaButton/CtaButton";
import AutomaticCounter from "../../../../components/AutomaticCounter/AutomaticCounter";
import { useViewportScroll } from "framer-motion"
import { getSourceUrl } from "../../../../utils/storageUtils";
import Process from "../../../../components/Process/Process";

const About = () => {

    const theme = useTheme();
    const { scrollY } = useViewportScroll()
    const isMobile = useMediaQuery(theme.breakpoints.down("sm"));

    const [processBackground, setProcessBackground] = useState('');
    const classes = useStyles({ processBackground })();
    const [startCounter, setStartCounter] = useState(false);
    const { t } = useTranslation();
    const about_title = t("about_title");
    const about_desc = t("about_desc");
    const about_cta = t("about_cta");
    const counterLits = useSelector(state => state.counters.data)
    const process_title = t("process_title");

    useEffect(() => {
        getSourceUrl('images/ourProcess.jpg', setProcessBackground);
    }, [])

    useEffect(() => {
        return scrollY.onChange((latest) => {
            let start = false
            if (isMobile) {
                if (latest >= 5468) {
                    start = true
                }
            } else {
                if (latest >= 2050) {
                    start = true
                }
            }
            if (start) {
                setStartCounter(true)
            }
        })
    }, [])

    return (
        <SectionContainer id="about_us"  paddingHorizontal={0}>
            <Box className={classes.container}>
                <Box className={classes.questionContainer}>
                    <Typography
                        custom={2}
                        variant="h2"
                        color='primary'
                        className={classes.question}
                    >
                        {about_title}
                    </Typography>
                    <Typography
                        custom={2}
                        variant="body2"
                        className={classes.answer}
                    >
                        {about_desc}
                    </Typography>
                    <div>
                        <CtaButton
                            ctaTitle={about_cta}
                        />
                    </div>

                </Box>
                <Box className={classes.counters}>
                    {counterLits.map(counter => (
                        <Box className={classes.counter}>
                            <AutomaticCounter
                                limit={counter.limit}
                                speed={counter.speed}
                                prefix={counter.prefix}
                                variant='h1'
                                className={classes.whiteText}
                                start={startCounter}
                            />
                            <Typography variant="h5" className={classes.whiteText} >
                                {counter.title[t('language')]}
                            </Typography>
                        </Box>

                    ))}

                </Box>
                <Box className={classes.ourProcess}>
                    <Typography variant="h1" className={classes.whiteText}>
                        {process_title}
                    </Typography>
                    <Process />
                </Box>
            </Box>
        </SectionContainer>
    );
};

const useStyles = props => makeStyles((theme) => ({
    container: {
        display: 'flex',
        flexDirection: 'column',
        width: '100%',
        gap: 0,
        height: 'auto',
        flexGrow: 1,
        paddingLeft: 0,
        paddingRight: 0,
        marginBottom: 50,
        [theme.breakpoints.down("sm")]: {
            width: 'auto',
        },
    },
    questionContainer: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        flexWrap: 'wrap',
        gap: 20,
        textAlign: 'center',
        width: '100%',
        paddingLeft:10,
        paddingRight:10
    },
    question: {
        [theme.breakpoints.down("sm")]: {
            fontSize: theme.typography.h2.fontSize,
        },
    },
    answer: {
        maxWidth: 600,
        [theme.breakpoints.down("sm")]: {
            fontSize: theme.typography.h6.fontSize,
        },
    },
    counters: {
        height: 'auto',
        width: '100%',
        backgroundColor: theme.palette.primary.main,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 25,
        gap: 25,
        [theme.breakpoints.down("sm")]: {
            flexDirection: 'column',
        },
    },
    counter: {
        height: 'auto',
        width: '30%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        [theme.breakpoints.down("sm")]: {
            width: '100%',
        },
    },
    whiteText: {
        color: theme.palette.white.main
    },
    ourProcess: {
        height: 'auto',
        width: '100%',
        background: `linear-gradient(0deg, rgba(4, 4, 4, 0.3), rgba(4, 4, 4, 0.3)), url(${props.processBackground})`,
        backgroundSize:'cover',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 25,
        gap: 25,
        [theme.breakpoints.down("sm")]: {
            flexDirection: 'column',
        },
    },
}));

export default About;
