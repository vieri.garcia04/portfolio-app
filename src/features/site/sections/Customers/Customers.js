import React, { useState } from "react";
import { Typography, makeStyles, Box, Container } from "@material-ui/core";
import SectionContainer from "../../../../containers/SectionContainer";
import { useTranslation } from "react-i18next";
import Testimonials from "../../../../components/Testimonials/Testimonials";

const Customers = () => {

    const classes = useStyles({})();
    const { t } = useTranslation();
    const customers_title = t("customers_title");
    const customers_desc = t("customers_desc");


    return (
        <SectionContainer id="customers_us"  paddingHorizontal={0}>
            <Container className={classes.container}>
                <Box className={classes.questionContainer}>
                    <Typography
                        custom={2}
                        variant="h2"
                        color='primary'
                        className={classes.question}
                    >
                        {customers_title}
                    </Typography>
                    <Typography
                        custom={2}
                        variant="body2"
                        className={classes.answer}
                    >
                        {customers_desc}
                    </Typography>

                </Box>
                <Testimonials />
            </Container>
        </SectionContainer>
    );
};

const useStyles = props => makeStyles((theme) => ({
    container: {
        display: 'flex',
        flexDirection: 'column',
        width: '100%',
        gap: 15,
        height: 'auto',
        paddingLeft: 0,
        paddingRight: 0,
        marginBottom: 50
    },
    questionContainer: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        flexWrap: 'wrap',
        gap: 20,
        textAlign: 'center',
        width: '100%',
        paddingLeft: 10,
        paddingRight: 10
    },
    question: {
        [theme.breakpoints.down("sm")]: {
            fontSize: theme.typography.h2.fontSize,
        },
    },
    answer: {
        maxWidth: 900,
        marginBottom: 30,
        [theme.breakpoints.down("sm")]: {
            fontSize: theme.typography.h6.fontSize,
        },
    },

    whiteText: {
        color: theme.palette.white.main
    },
    testimonials: {
        height: 'auto',
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 25,
        gap: 25,
        [theme.breakpoints.down("sm")]: {
            flexDirection: 'column',
        },
    }
}));

export default Customers;
