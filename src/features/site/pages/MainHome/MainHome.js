import React from "react";
import { motion } from "framer-motion";
import { useMediaQuery, useTheme } from "@material-ui/core";

// import Experience from "../../sections/Experience";
// import Projects from "../../sections/Projects";
import Contact from "../../sections/Contact";
import Home from "../../sections/Home";
import About from "../../sections/About"
import Loader from "../../../../components/Loader";
import Navbar from "../../../../components/Navbar";
import Footer from "../../../../components/Footer";
import Social from "../../../../components/Social";
import Services from "../../sections/Services";
import Customers from "../../sections/Customers/Customers";
import Projects from "../../sections/Projects/Projects";
//import Background3d from "../../components/Background3d.js/Background3d";
//import {  useTheme } from "@material-ui/core";

const MainHome = () => {
    const theme = useTheme();
    const isMobile = useMediaQuery(theme.breakpoints.down("md"));
    return (
        <>
            <Loader/>
            <Navbar/>
            {!isMobile && <Social/>}
            <motion.main
                initial={{ opacity: 0 }}
                animate={{ opacity: 1 }}
                transition={{
                    type: "spring",
                    stiffness: 260,
                    damping: 20,
                }}
            >
                <Home />
                <Services/>
                <About/>
                <Customers/>
                <Projects/>
                <Contact />

            </motion.main>
            <Footer/>
        </>

    );
};

export default MainHome;
