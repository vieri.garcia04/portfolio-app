import { createSlice } from '@reduxjs/toolkit'

export const labelSlice = createSlice(
    {
        name: 'labels',
        initialState: {
        },
        reducers: {
            setLabels: (state, action) => {
                const { payload } = action
                state = { state, ...payload };
            }
        }
    }
)
export const { setLabels } = labelSlice.actions
export default labelSlice.reducer