import { configureStore } from '@reduxjs/toolkit'
import socialReducer from '../components/Social/socialSlice'
import languagesReducer from '../components/Navbar/languageSlice'
import benefitsReducer from '../components/Benefits/benefitSlice'
import skillReducer from '../components/Skills/skillSlice'
import reasonReducer from '../components/Reasons/reasonSlice'
import servicesReducer from '../components/Services/serviceSlice'
import counterReducer from '../features/site/sections/About/counterSlice'
import processReducer from '../components/Process/processSlice'
import customersReducer from '../components/Testimonials/customersSlice'

export const store = configureStore({
  reducer: {
    socials: socialReducer,
    languages: languagesReducer,
    benefits: benefitsReducer,
    skills: skillReducer,
    reasons: reasonReducer,
    services: servicesReducer,
    counters: counterReducer,
    process: processReducer,
    customers: customersReducer
  },
})