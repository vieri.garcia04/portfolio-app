// eslint-disable-next-line
const fontFactor = .7;
const fontFactorBody =.9;
const fontFactorTitle =1;
const fontFactorLabel = 1;
export default {
    fontFamily: "Roboto",
    h1: {
        fontWeight: 500,
        fontSize: fontFactor * 57,
    },
    h2: {
        fontWeight: 500,
        fontSize: fontFactor * 45,
    },
    h3: {
        fontWeight: 500,
        fontSize: fontFactor * 36,
    },
    h4: {
        fontWeight: 500,
        fontSize: fontFactor * 32,
    },
    h5: {
        fontWeight: 500,
        fontSize: fontFactor * 28,
    },
    h6: {
        fontWeight: 400,
        fontSize: fontFactor * 24,
    },
    titleLarge: {
        fontWeight: 500,
        fontSize: fontFactorTitle * 22,
    },
    titleMedium: {
        fontWeight: 500,
        fontSize: fontFactorTitle * 16,
    },
    titleSmall: {
        fontWeight: 500,
        fontSize: fontFactorTitle * 14,
    },
    labelLarge: {
        fontWeight: 500,
        fontSize: fontFactorLabel * 14,
    },
    labelMedium: {
        fontWeight: 400,
        fontSize: fontFactorLabel * 12,
    },
    labelSmall: {
        fontWeight: 400,
        fontSize: fontFactorLabel * 11,
    },
    bodyLarge: {
        fontWeight: 300,
        fontSize: fontFactorBody * 18,
    },
    bodyMedium: {
        fontWeight: 'regular',
        fontSize: fontFactorBody * 16,
    },
    bodySmall: {
        fontWeight: 'light',
        fontSize: fontFactorBody * 14,
    },
    overline: {
        fontWeight: 500,
    },
    body1: {
        fontWeight: 300,
        fontSize: fontFactorBody * 14,
    },
    body2: {
        fontWeight: 300,
        fontSize: fontFactorBody * 18,
    },
};
