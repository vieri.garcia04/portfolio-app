import { createTheme } from "@material-ui/core";
import typography from "./typography";

const baseTheme = {
    breakpoints: {
        values: {
            xs: 0,
            sm: 700,
            md: 960,
            lg: 1320,
            xl: 1920,
        },
    },
    navbarHeight: "90px",
    mobileNavbarHeight: "80px",
    typography,
    overrides: {
        MuiButton: {
            root: {
                textTransform: "none",
                fontSize: "16px",
            },
            text: {
                letterSpacing: "2px",
                borderRadius: 0,
                "&:hover": {
                    color: "rgb(230,230,230)",
                    backgroundColor: "inherit",
                },
            },
        },
    },
};

const darkTheme = createTheme({
    palette: {
        basicColor:{
            primary:'#130748',
            secondary:'#FF9C00',
            white:'#FFFFFF',
            black:'#000000'
        },
        background: {
            default: '#ffffff',
             paper:'#FFE07A'
        },
        primary: {
            main: '#21188D',
            contrastText: '#ffffff',
        },
        secondary: {
            main: "#FF9C00",
            contrastText: "#FFE07A",
        },
        surfaceVariant:{
            main: "#e4e1ec",
            contrastText: "#47464f",
        },
        text: {
            primary: "#000",
            secondary: "rgb(30,30,30)"
        },
        primaryContainer:{
            main:'#CDC9FF',
            contrastText:'#0f0366'
        },
        action: {
            disabled: "rgb(70,70,70)",
            disabledBackground: "rgb(150,150,150)",
        },
        white:{
            contrastText: '#000000',
            main: '#FFFFFF',
        },
        black:{
            main: '#000000',
            contrastText: '#FFFFFF',
        },
        error:{
            main:'#FF9C00'
        }
    },
    logoColor:"#fff",
    ...baseTheme
});

const lightTheme = createTheme({
    palette: {
        basicColor:{
            primary:'#130748',
            secondary:'#FF9C00',
            white:'#FFFFFF',
            black:'#000000'
        },
        background: {
            default: '#ffffff',
             paper:'#FFE07A'
        },
        primary: {
            main: '#21188D',
            contrastText: '#ffffff',
        },
        secondary: {
            main: "#FF9C00",
            contrastText: "#FFE07A",
        },
        surfaceVariant:{
            main: "#e4e1ec",
            contrastText: "#47464f",
        },
        text: {
            primary: "#000",
            secondary: "rgb(30,30,30)"
        },
        primaryContainer:{
            main:'#CDC9FF',
            contrastText:'#0f0366'
        },
        action: {
            disabled: "rgb(70,70,70)",
            disabledBackground: "rgb(150,150,150)",
        },
        white:{
            contrastText: '#000000',
            main: '#FFFFFF',
        },
        black:{
            main: '#000000',
            contrastText: '#FFFFFF',
        },
        error:{
            main:'#FF9C00'
        }
    },
    backgroundSecondary: {
        bg : "#6F4C5B",
        text: "rgb(230,230,230) "
    },
    logoColor:"#6F4C5B",
    
    ...baseTheme
});

export { darkTheme, lightTheme };


// {'lightColors': {'primary': '#5654aa',
//   'onPrimary': '#ffffff',
//   'primaryContainer': '#e2dfff',
//   'onPrimaryContainer': '#0f0366',
//   'secondary': '#845400',
//   'onSecondary': '#ffffff',
//   'secondaryContainer': '#ffddb6',
//   'onSecondaryContainer': '#2a1800',
//   'tertiary': '#7a5368',
//   'onTertiary': '#ffffff',
//   'tertiaryContainer': '#ffd8eb',
//   'onTertiaryContainer': '#2f1124',
//   'error': '#ba1a1a',
//   'errorContainer': '#ffdad6',
//   'onError': '#ffffff',
//   'onErrorContainer': '#410002',
//   'background': '#fffbff',
//   'onBackground': '#1c1b1f',
//   'surface': '#fffbff',
//   'onSurface': '#1c1b1f',
//   'surfaceVariant': '#e4e1ec',
//   'onSurfaceVariant': '#47464f',
//   'outline': '#787680',
//   'inverseOnSurface': '#f3eff4',
//   'inverseSurface': '#313034',
//   'inversePrimary': '#c2c1ff',
//   'shadow': '#000000',
//   'surfaceTint': '#5654aa',
//   'surfaceTintColor': '#5654aa'},
//  'darkColors': {'primary': '#c2c1ff',
//   'onPrimary': '#272379',
//   'primaryContainer': '#3e3c91',
//   'onPrimaryContainer': '#e2dfff',
//   'secondary': '#ffb95b',
//   'onSecondary': '#462a00',
//   'secondaryContainer': '#643f00',
//   'onSecondaryContainer': '#ffddb6',
//   'tertiary': '#eab9d2',
//   'onTertiary': '#472639',
//   'tertiaryContainer': '#603c50',
//   'onTertiaryContainer': '#ffd8eb',
//   'error': '#ffb4ab',
//   'errorContainer': '#93000a',
//   'onError': '#690005',
//   'onErrorContainer': '#ffdad6',
//   'background': '#1c1b1f',
//   'onBackground': '#e5e1e6',
//   'surface': '#1c1b1f',
//   'onSurface': '#e5e1e6',
//   'surfaceVariant': '#47464f',
//   'onSurfaceVariant': '#c8c5d0',
//   'outline': '#928f9a',
//   'inverseOnSurface': '#1c1b1f',
//   'inverseSurface': '#e5e1e6',
//   'inversePrimary': '#5654aa',
//   'shadow': '#000000',
//   'surfaceTint': '#c2c1ff',
//   'surfaceTintColor': '#c2c1ff'},
//  'fontFamily': 'Segoe UI'}
