// Import the functions you need from the SDKs you need
import { initializeApp} from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getFirestore } from "firebase/firestore";
import { getStorage } from "firebase/storage";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyCIII70siYSXOQxDctJ4h0-3SweCdJEUfk",
  authDomain: "vierigarcia-blog.firebaseapp.com",
  projectId: "vierigarcia-blog",
  storageBucket: "vierigarcia-blog.appspot.com",
  messagingSenderId: "275040690502",
  appId: "1:275040690502:web:c0366bf56953632f38c547",
  measurementId: "G-J6BP6DRFQY"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);
export const db = getFirestore(app);
export const storage = getStorage(app);